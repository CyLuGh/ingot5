﻿using Ingots.ViewModels;
using ReactiveUI;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.Views
{
    public partial class BankView
    {
        public BankView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(x => PopulateFromViewModel(this, x, disposables))
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private static void PopulateFromViewModel(BankView view, BankViewModel viewModel, CompositeDisposable disposables)
        {
            view.OneWayBind(viewModel,
                vm => vm.Name,
                v => v.BankTextBlock.Text)
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                    vm => vm.TotalValue,
                    v => v.TotalTextBlock.Text,
                    v => v.ToString("C2"))
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                vm => vm.Accounts,
                v => v.AccountsListBox.ItemsSource)
                .DisposeWith(disposables);

            view.Bind(viewModel,
                vm => vm.SelectedAccount,
                v => v.AccountsListBox.SelectedItem)
                .DisposeWith(disposables);
        }
    }
}