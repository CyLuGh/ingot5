﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ingots.Data.Migrations
{
    public partial class AddBic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Bic",
                table: "Accounts",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bic",
                table: "Accounts");
        }
    }
}
