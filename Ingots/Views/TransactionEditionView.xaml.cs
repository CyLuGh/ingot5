﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.Views
{
    public partial class TransactionEditionView
    {
        public TransactionEditionView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .Subscribe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( TransactionEditionView view , TransactionEditionViewModel viewModel , CompositeDisposable disposables )
        {
            view.Bind( viewModel ,
                vm => vm.Category ,
                v => v.ComboBoxCategory.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.SubCategory ,
                v => v.ComboBoxSubcategory.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Shop ,
                v => v.ComboBoxShop.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Categories ,
                v => v.ComboBoxCategory.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.SubCategories ,
                v => v.ComboBoxSubcategory.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Shops ,
                v => v.ComboBoxShop.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Category ,
                v => v.ComboBoxSubcategory.IsEnabled ,
                c => !string.IsNullOrWhiteSpace( c ) )
                .DisposeWith( disposables );
        }
    }
}