﻿namespace Ingots.Models
{
    public class Transfer : Operation
    {
        public int TargetAccountId { get; set; }
        public Account? TargetAccount { get; set; }

        public override string ToString()
            => $"TRANSFER {Date:yyyy-MM-dd} {Description} {Value:N2}";
    }
}