﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.Views
{
    public partial class TransactionDataGridView
    {
        public TransactionDataGridView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( TransactionDataGridView view , TransactionViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.Category,
                v => v.TextBoxCategory.Text)
                .DisposeWith(disposables);
                
            view.OneWayBind( viewModel ,
                vm => vm.SubCategory,
                v => v.TextBoxSubCategory.Text)
                .DisposeWith(disposables);

            view.OneWayBind( viewModel ,
                vm => vm.Shop,
                v => v.TextBoxShop.Text)
                .DisposeWith(disposables);
        }
    }
}