﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using CsvHelper;
using DynamicData;
using DynamicData.Binding;
using Ingots.Models;
using Ingots.ViewModels.Charts;
using Ingots.ViewModels.Infrastructure;
using LanguageExt;
using Mapster;
using MapsterMapper;
using OxyPlot;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace Ingots.ViewModels
{
    public class BankAccountsViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }
        public IngotsViewModel IngotsViewModel { get; }

        private ReadOnlyObservableCollection<BankViewModel> _banks;
        public ReadOnlyObservableCollection<BankViewModel> Banks => _banks;

        private ReadOnlyObservableCollection<OperationViewModel> _operations;
        public ReadOnlyObservableCollection<OperationViewModel> Operations => _operations;

        [Reactive] public AccountViewModel SelectedAccount { get; set; }
        [Reactive] public OperationViewModel SelectedOperation { get; set; }
        [Reactive] public string SearchInput { get; set; }

        /* Account creation */
        public ReactiveCommand<System.Reactive.Unit , Option<AccountEditionViewModel>> CreateAccountCommand { get; private set; }

        public Interaction<System.Reactive.Unit , Option<AccountEditionViewModel>> CreateAccountInteraction { get; }
            = new Interaction<System.Reactive.Unit , Option<AccountEditionViewModel>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<AccountEditionViewModel , Option<AccountViewModel>> AddAccountCommand { get; private set; }

        /* Account edition */
        public ReactiveCommand<AccountViewModel , Option<AccountEditionViewModel>> EditAccountCommand { get; private set; }

        public Interaction<AccountViewModel , Option<AccountEditionViewModel>> EditAccountInteraction { get; }
            = new Interaction<AccountViewModel , Option<AccountEditionViewModel>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<AccountEditionViewModel , Option<AccountViewModel>> UpdateAccountCommand { get; private set; }

        /* Account deletion */
        public ReactiveCommand<AccountViewModel , Option<AccountEditionViewModel>> DeleteAccountCommand { get; private set; }

        public Interaction<AccountViewModel , Option<AccountEditionViewModel>> DeleteAccountInteraction { get; }
            = new Interaction<AccountViewModel , Option<AccountEditionViewModel>>( RxApp.MainThreadScheduler );

        /* Transaction creation */
        public ReactiveCommand<AccountViewModel , Option<TransactionViewModel>> CreateTransactionCommand { get; private set; }

        public Interaction<TransactionEditionViewModel , Option<TransactionViewModel>> CreateTransactionInteraction { get; }
            = new Interaction<TransactionEditionViewModel , Option<TransactionViewModel>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<TransactionViewModel , Option<TransactionViewModel>> AddTransactionCommand { get; private set; }

        /* Transfer creation */
        public ReactiveCommand<AccountViewModel , Option<TransferViewModel>> CreateTransferCommand { get; private set; }

        public Interaction<TransferEditionViewModel , Option<TransferViewModel>> CreateTransferInteraction { get; }
            = new Interaction<TransferEditionViewModel , Option<TransferViewModel>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<TransferViewModel , Option<TransferViewModel>> AddTransferCommand { get; private set; }

        /* Operation Edition */
        public ReactiveCommand<OperationViewModel , Option<OperationViewModel>> EditOperationCommand { get; private set; }

        public Interaction<OperationEditionViewModel , Option<OperationViewModel>> EditOperationInteraction { get; }
            = new Interaction<OperationEditionViewModel , Option<OperationViewModel>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<OperationViewModel , Option<OperationViewModel>> UpdateOperationCommand { get; private set; }
        public ReactiveCommand<OperationViewModel , Option<OperationViewModel>> EraseOperationCommand { get; private set; }

        /* Operation deletion */
        public ReactiveCommand<OperationViewModel , Option<OperationViewModel>> DeleteOperationCommand { get; private set; }

        public Interaction<OperationEditionViewModel , Option<OperationViewModel>> DeleteOperationInteaction { get; }
            = new Interaction<OperationEditionViewModel , Option<OperationViewModel>>( RxApp.MainThreadScheduler );

        /* CSV */
        public ReactiveCommand<System.Reactive.Unit , Option<string>> PickCsvFileCommand { get; private set; }

        public Interaction<System.Reactive.Unit , Option<string>> PickCsvFileInteraction { get; }
            = new Interaction<System.Reactive.Unit , Option<string>>( RxApp.MainThreadScheduler );

        public ReactiveCommand<string , OperationViewModel[]> ParseCsvContentCommand { get; private set; }

        public ReactiveCommand<OperationViewModel[] , OperationViewModel[]> ChooseCsvContentCommand { get; private set; }

        public Interaction<OperationViewModel[] , OperationViewModel[]> ChooseCsvContentInteraction { get; }
            = new Interaction<OperationViewModel[] , OperationViewModel[]>( RxApp.MainThreadScheduler );

        /* Chart */
        public ReactiveCommand<OperationViewModel[] , ChartSeries> BuildChartSeriesCommand { get; private set; }

        public Interaction<ChartSeries , Unit> DrawChartInteraction { get; }
            = new Interaction<ChartSeries , Unit>( RxApp.MainThreadScheduler );

        public PageParameterData PageParameters { get; } = new PageParameterData( 1 , 200 );

        public BankAccountsViewModel( IngotsViewModel ingotsViewModel )
        {
            Activator = new ViewModelActivator();
            IngotsViewModel = ingotsViewModel;
            InitializeCommands( this );

            var pager = PageParameters.WhenChanged( vm => vm.PageSize , vm => vm.CurrentPage ,
                                          ( _ , size , page ) => new PageRequest( page , size ) )
                                      .StartWith( new PageRequest( 1 , 200 ) )
                                      .DistinctUntilChanged()
                                      .Throttle( TimeSpan.FromMilliseconds( 75 ) )
                                      .ObserveOn( RxApp.TaskpoolScheduler );

            var filter = this.WhenAnyValue( x => x.SelectedAccount )
                             .Select( _ =>
                                  new Func<OperationViewModel , bool>( ovm => SelectedAccount != null && ovm.AccountId == SelectedAccount.AccountId ) );

            var searchFilter = this.WhenAnyValue( x => x.SearchInput )
                                   .Select( CreateSearchFilter );

            CreateAccountCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( AddAccountCommand ) );
            AddAccountCommand.SubscribeSafe( opt => opt.ToObservable().SubscribeSafe( avm => IngotsViewModel.AccountsCache.AddOrUpdate( avm ) ) );
            EditAccountCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( UpdateAccountCommand ) );
            UpdateAccountCommand.SubscribeSafe( opt => opt.ToObservable().SubscribeSafe( avm => IngotsViewModel.AccountsCache.AddOrUpdate( avm ) ) );
            DeleteAccountCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( UpdateAccountCommand ) );

            CreateTransactionCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( AddTransactionCommand ) );
            AddTransactionCommand.SubscribeSafe( opt => opt.ToObservable().SubscribeSafe( tvm => IngotsViewModel.TransactionsCache.AddOrUpdate( tvm ) ) );

            CreateTransferCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( AddTransferCommand ) );
            AddTransferCommand.SubscribeSafe( opt => opt.ToObservable().SubscribeSafe( tvm => IngotsViewModel.TransfersCache.AddOrUpdate( tvm ) ) );

            EditOperationCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( UpdateOperationCommand ) );
            UpdateOperationCommand.Subscribe( opt => opt.ToObservable().SubscribeSafe( ovm =>
            {
                if ( ovm is TransactionViewModel transaction )
                    IngotsViewModel.TransactionsCache.AddOrUpdate( transaction );
                else if ( ovm is TransferViewModel transfer )
                    IngotsViewModel.TransfersCache.AddOrUpdate( transfer );
            } ) );

            DeleteOperationCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( EraseOperationCommand ) );
            EraseOperationCommand.Subscribe( opt => opt.ToObservable().SubscribeSafe( ovm =>
            {
                if ( ovm is TransactionViewModel transaction )
                    IngotsViewModel.TransactionsCache.Remove( transaction );
                else if ( ovm is TransferViewModel transfer )
                    IngotsViewModel.TransfersCache.Remove( transfer );
            } ) );

            PickCsvFileCommand.SubscribeSafe( opt => opt.ToObservable().InvokeCommand( ParseCsvContentCommand ) );

            ParseCsvContentCommand.Where( x => x?.Any() == true )
                .InvokeCommand( ChooseCsvContentCommand );

            this.WhenActivated( disposables =>
            {
                Observable.Merge(
                    CreateAccountCommand.ThrownExceptions.Select( ex => ("Couldn't create account", ex) ) ,
                    AddAccountCommand.ThrownExceptions.Select( ex => ("Couldn't add account", ex) ) ,
                    EditAccountCommand.ThrownExceptions.Select( ex => ("Couldn't edit account", ex) ) ,
                    UpdateAccountCommand.ThrownExceptions.Select( ex => ("Couldn't update account", ex) ) ,
                    DeleteAccountCommand.ThrownExceptions.Select( ex => ("Couldn't delete account", ex) ) ,
                    CreateTransactionCommand.ThrownExceptions.Select( ex => ("Couldn't create transaction", ex) ) ,
                    AddTransactionCommand.ThrownExceptions.Select( ex => ("Couldn't add transaction", ex) ) ,
                    CreateTransferCommand.ThrownExceptions.Select( ex => ("Couldn't create transfer", ex) ) ,
                    AddTransferCommand.ThrownExceptions.Select( ex => ("Couldn't add transfer", ex) ) ,
                    EditOperationCommand.ThrownExceptions.Select( ex => ("Couldn't edit operation", ex) ) ,
                    UpdateOperationCommand.ThrownExceptions.Select( ex => ("Couldn't edit operation", ex) ) ,
                    DeleteOperationCommand.ThrownExceptions.Select( ex => ("Couldn't delete operation", ex) ) ,
                    EraseOperationCommand.ThrownExceptions.Select( ex => ("Couldn't erase operation", ex) ) ,
                    BuildChartSeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build chart series", ex) ) ,
                    PickCsvFileCommand.ThrownExceptions.Select( ex => ("Couldn't pick CSV file", ex) ) ,
                    ParseCsvContentCommand.ThrownExceptions.Select( ex => ("Couldn't parse CSV file", ex) ) ,
                    ChooseCsvContentCommand.ThrownExceptions.Select( ex => ("Couldn't choose elements in CSV", ex) )
                )
                .SubscribeSafe( t =>
                {
                    var (title, exception) = t;
                    this.Log().Error( $"{title}: {exception}" );
                } )
                .DisposeWith( disposables );

                var operationsCache = IngotsViewModel.TransactionsCache.Connect()
                                        .Transform( t => t as OperationViewModel )
                                        .Or( IngotsViewModel.TransfersCache.Connect()
                                                .AutoRefresh( x => x.IsExecuted )
                                                .TransformMany( t => new[]
                                                    {
                                                        t as OperationViewModel,
                                                        t.OppositeTransfer() as OperationViewModel
                                                    } ,
                                                    t => (t.OperationId, t.IsDerived, typeof( TransferViewModel )) ) )
                                                .Filter( filter )
                                                .AsObservableCache()
                                                .DisposeWith( disposables );

                operationsCache.Connect()
                    .Filter( searchFilter )
                    .Sort( SortExpressionComparer<OperationViewModel>.Descending( x => x.Date ) )
                    .Page( pager )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Do( changes => PageParameters.Update( changes.Response ) )
                    .Bind( out _operations )
                    .DisposeMany()
                    .SubscribeSafe()
                    .DisposeWith( disposables );

                operationsCache.Connect().AutoRefresh()
                    .CombineLatest( ingotsViewModel.AccountsCache.Connect().WhenValueChanged( x => x.StartValue ) ,
                        ( _ , __ ) => operationsCache.Items.ToArray() )
                        .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                        .InvokeCommand( BuildChartSeriesCommand )
                        .DisposeWith( disposables );

                ingotsViewModel.AccountsCache
                    .Connect()
                    .Filter( a => !a.IsDeleted )
                    .Sort( SortExpressionComparer<AccountViewModel>.Ascending( a => a.Bank ) )
                    .Group( a => a.Bank )
                    .Transform( g => new BankViewModel( this , g.Cache ) { Name = g.Key } )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _banks )
                    .DisposeMany()
                    .SubscribeSafe()
                    .DisposeWith( disposables );

                BuildChartSeriesCommand.SubscribeSafe( async series => await DrawChartInteraction.Handle( series ) )
                    .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( BankAccountsViewModel @this )
        {
            @this.CreateAccountCommand = ReactiveCommand.CreateFromObservable(
                () => @this.CreateAccountInteraction.Handle( System.Reactive.Unit.Default ) );

            @this.AddAccountCommand = ReactiveCommand.CreateFromObservable<AccountEditionViewModel , Option<AccountViewModel>>( aevm =>
                  Observable.Start( () =>
                       @this.IngotsViewModel.Context
                           .Some( ctx =>
                           {
                               var acc = aevm.Adapt<Account>();
                               var added = ctx.Accounts.Add( acc );
                               ctx.SaveChanges();

                               var avm = added.Entity.Adapt<AccountViewModel>();

                               return Option<AccountViewModel>.Some( avm );
                           } )
                           .None( () => Option<AccountViewModel>.None )
                 ) );

            var hasSelectedAccount = @this.WhenAnyValue( x => x.SelectedAccount )
                .Select( sa => sa != null )
                .ObserveOn( RxApp.MainThreadScheduler );
            @this.EditAccountCommand = ReactiveCommand.CreateFromObservable<AccountViewModel , Option<AccountEditionViewModel>>(
                avm => @this.EditAccountInteraction.Handle( avm ) , hasSelectedAccount );

            @this.UpdateAccountCommand = ReactiveCommand.CreateFromObservable<AccountEditionViewModel , Option<AccountViewModel>>( aevm =>
                   Observable.Start( () =>
                        @this.IngotsViewModel.Context
                            .Some( ctx =>
                            {
                                // Retrieve account in db
                                var acc = ctx.Accounts.FirstOrDefault( x => x.AccountId == aevm.AccountId );
                                if ( acc != null )
                                {
                                    // Apply changes from editor
                                    new Mapper().Map<AccountEditionViewModel , Account>( aevm , acc );
                                    ctx.SaveChanges();
                                    var avm = acc.Adapt<AccountViewModel>();
                                    avm.OperationsValue = @this.IngotsViewModel.TransactionsCache.Items.Where( x => x.AccountId == avm.AccountId ).Sum( x => x.Value )
                                        + @this.IngotsViewModel.TransfersCache.Items.Where( x => x.AccountId == avm.AccountId ).Sum( x => x.Value )
                                        - @this.IngotsViewModel.TransfersCache.Items.Where( x => x.TargetAccountId == avm.AccountId ).Sum( x => x.Value );
                                    return Option<AccountViewModel>.Some( avm );
                                }
                                else
                                {
                                    return Option<AccountViewModel>.None;
                                }
                            } )
                            .None( () => Option<AccountViewModel>.None )
                  ) );

            @this.DeleteAccountCommand = ReactiveCommand.CreateFromObservable<AccountViewModel , Option<AccountEditionViewModel>>(
                avm => @this.DeleteAccountInteraction.Handle( avm ) , hasSelectedAccount );

            @this.CreateTransactionCommand = ReactiveCommand.CreateFromObservable<AccountViewModel , Option<TransactionViewModel>>(
                avm =>
                {
                    var transactions = @this.IngotsViewModel.TransactionsCache.Items.ToArray();

                    var tevm = new TransactionEditionViewModel
                    {
                        AccountId = avm.AccountId ,
                        ValueInput = 0.ToString( "C2" ) ,
                        Date = DateTime.Today ,
                        ExistingShops = transactions.Select( o => o.Shop ).Distinct().ToArray() ,
                        ExistingCategories = transactions.GroupBy( o => o.Category )
                                                    .ToDictionary( g => g.Key , g => g.Select( o => o.SubCategory ).Distinct().ToArray() )
                    };
                    return @this.CreateTransactionInteraction.Handle( tevm );
                } , hasSelectedAccount );

            @this.AddTransactionCommand = ReactiveCommand.CreateFromObservable<TransactionViewModel , Option<TransactionViewModel>>(
                tvm => Observable.Start( () => @this.IngotsViewModel.Context
                           .Some( ctx =>
                            {
                                var transaction = tvm.Adapt<Transaction>();
                                var added = ctx.Transactions.Add( transaction );
                                ctx.SaveChanges();

                                var res = added.Entity.Adapt<TransactionViewModel>();
                                return Option<TransactionViewModel>.Some( res );
                            } )
                           .None( () => Option<TransactionViewModel>.None ) ) );

            @this.CreateTransferCommand = ReactiveCommand.CreateFromObservable<AccountViewModel , Option<TransferViewModel>>(
                avm =>
                {
                    var tevm = new TransferEditionViewModel
                    {
                        AccountId = avm.AccountId ,
                        ValueInput = 0.ToString( "C2" ) ,
                        Date = DateTime.Today ,
                        Accounts = @this.IngotsViewModel.AccountsCache.Items.ToArray()
                    };
                    return @this.CreateTransferInteraction.Handle( tevm );
                } , hasSelectedAccount );

            @this.AddTransferCommand = ReactiveCommand.CreateFromObservable<TransferViewModel , Option<TransferViewModel>>(
                tvm => Observable.Start( () => @this.IngotsViewModel.Context
                           .Some( ctx =>
                            {
                                var transfer = tvm.Adapt<Transfer>();
                                var added = ctx.Transfers.Add( transfer );
                                ctx.SaveChanges();

                                var res = added.Entity.Adapt<TransferViewModel>();

                                return Option<TransferViewModel>.Some( res );
                            } )
                           .None( () => Option<TransferViewModel>.None ) ) );

            @this.BuildChartSeriesCommand = ReactiveCommand.CreateFromObservable<OperationViewModel[] , ChartSeries>(
                operations => Observable.Start( () =>
                {
                    var data = new LinkedList<(DateTime date, double amount)>();

                    var _ = operations.Where( o => o.IsExecuted )
                                .OrderBy( o => o.Date )
                                .GroupBy( o => o.Date )
                                .Aggregate( data , ( list , current ) =>
                                {
                                    var previous = list.Count == 0 ? ( @this.SelectedAccount?.StartValue ?? 0d ) : list.Last.Value.amount;
                                    list.AddLast( (current.Key, previous + current.Sum( o => o.Value )) );
                                    return list;
                                } );

                    if ( data.Count > 0 )
                    {
                        var (date, amount) = data.Last.Value;
                        if ( date < DateTime.Today )
                            data.AddLast( (DateTime.Today, amount) );
                    }

                    return new ChartSeries
                    {
                        Title = @this.SelectedAccount?.Description ?? string.Empty ,
                        DataSource = data.ToDictionary( o => o.date , o => o.amount )
                    };
                } ) );

            var hasSelectedOperation = @this.WhenAnyValue( x => x.SelectedOperation )
                .Select( so => so != null )
                .ObserveOn( RxApp.MainThreadScheduler );

            @this.EditOperationCommand = ReactiveCommand.CreateFromObservable<OperationViewModel , Option<OperationViewModel>>(
                ovm =>
                {
                    if ( ovm is TransactionViewModel transaction )
                    {
                        TypeAdapterConfig<TransactionViewModel , TransactionEditionViewModel>.NewConfig()
                            .Map( dest => dest.ValueInput , src => src.Value.ToString( "C2" ) );

                        var tevm = transaction.Adapt<TransactionEditionViewModel>();
                        var transactions = @this.IngotsViewModel.TransactionsCache.Items.ToArray();

                        tevm.ExistingShops = transactions.Select( o => o.Shop ).Distinct().ToArray();
                        tevm.ExistingCategories = transactions.GroupBy( o => o.Category )
                                                    .ToDictionary( g => g.Key , g => g.Select( o => o.SubCategory ).Distinct().ToArray() );
                        return @this.EditOperationInteraction.Handle( tevm );
                    }
                    else if ( ovm is TransferViewModel transfer )
                    {
                        var tevm = new TransferEditionViewModel
                        {
                            Accounts = @this.IngotsViewModel.AccountsCache.Items.ToArray() ,
                            PreviousTarget = Option<AccountViewModel>.Some( @this.IngotsViewModel.AccountsCache.Items.First( x => x.AccountId == transfer.TargetAccountId ) )
                        };

                        TypeAdapterConfig<TransferViewModel , TransferEditionViewModel>.NewConfig()
                            .Map( dest => dest.ValueInput , src => src.Value.ToString( "C2" ) )
                            .Map( dest => dest.SelectedTarget ,
                                    src => @this.IngotsViewModel.AccountsCache.Items
                                    .FirstOrDefault( x => x.AccountId == src.TargetAccountId ) );
                        new Mapper().Map( transfer , tevm );
                        return @this.EditOperationInteraction.Handle( tevm );
                    }

                    return Observable.Start( () => Option<OperationViewModel>.None );
                } , hasSelectedOperation );

            @this.UpdateOperationCommand = ReactiveCommand
                .CreateFromObservable<OperationViewModel , Option<OperationViewModel>>( oevm =>
                    Observable.Start( () => @this.IngotsViewModel.Context
                        .Some( ctx =>
                        {
                            if ( oevm is TransactionViewModel transaction )
                            {
                                var tr = ctx.Transactions.FirstOrDefault( x => x.OperationId == transaction.OperationId );
                                if ( tr != null )
                                {
                                    // Apply changes from editor
                                    new Mapper().Map( transaction , tr );
                                    ctx.SaveChanges();
                                    var tvm = tr.Adapt<TransactionViewModel>();
                                    return Option<OperationViewModel>.Some( tvm );
                                }
                            }
                            else if ( oevm is TransferViewModel transfer )
                            {
                                var tr = ctx.Transfers.FirstOrDefault( x => x.OperationId == transfer.OperationId );
                                if ( tr != null )
                                {
                                    // Apply changes from editor
                                    new Mapper().Map( transfer , tr );
                                    ctx.SaveChanges();
                                    var tvm = tr.Adapt<TransferViewModel>();
                                    return Option<OperationViewModel>.Some( tvm );
                                }
                            }

                            return Option<OperationViewModel>.None;
                        } )
                        .None( () => Option<OperationViewModel>.None ) ) );

            @this.DeleteOperationCommand = ReactiveCommand.CreateFromObservable<OperationViewModel , Option<OperationViewModel>>(
                ovm =>
                {
                    if ( ovm is TransactionViewModel transaction )
                    {
                        TypeAdapterConfig<TransactionViewModel , TransactionEditionViewModel>.NewConfig()
                            .Map( dest => dest.ValueInput , src => src.Value.ToString( "C2" ) );

                        var tevm = transaction.Adapt<TransactionEditionViewModel>();
                        var transactions = @this.Operations.OfType<TransactionViewModel>().ToArray();

                        tevm.ExistingShops = transactions.Select( o => o.Shop ).Distinct().ToArray();
                        tevm.ExistingCategories = transactions.GroupBy( o => o.Category )
                                                    .ToDictionary( g => g.Key , g => g.Select( o => o.SubCategory ).Distinct().ToArray() );
                        return @this.DeleteOperationInteaction.Handle( tevm );
                    }
                    else if ( ovm is TransferViewModel transfer )
                    {
                        var tevm = new TransferEditionViewModel
                        {
                            Accounts = @this.IngotsViewModel.AccountsCache.Items.ToArray() ,
                            PreviousTarget = Option<AccountViewModel>.Some( @this.IngotsViewModel.AccountsCache.Items.First( x => x.AccountId == transfer.TargetAccountId ) )
                        };

                        TypeAdapterConfig<TransferViewModel , TransferEditionViewModel>.NewConfig()
                            .Map( dest => dest.ValueInput , src => src.Value.ToString( "C2" ) )
                            .Map( dest => dest.SelectedTarget ,
                                    src => @this.IngotsViewModel.AccountsCache.Items
                                    .FirstOrDefault( x => x.AccountId == src.TargetAccountId ) );
                        new Mapper().Map( transfer , tevm );
                        return @this.DeleteOperationInteaction.Handle( tevm );
                    }

                    return Observable.Start( () => Option<OperationViewModel>.None );
                } , hasSelectedOperation );

            @this.EraseOperationCommand = ReactiveCommand.CreateFromObservable<OperationViewModel , Option<OperationViewModel>>(
                ovm => Observable.Start( () => @this.IngotsViewModel.Context
                     .Some( ctx =>
                     {
                         if ( ovm is TransactionViewModel transaction )
                         {
                             var tr = ctx.Transactions.FirstOrDefault( x => x.OperationId == transaction.OperationId );
                             if ( tr != null )
                             {
                                 // Apply changes from editor
                                 new Mapper().Map( transaction , tr );
                                 ctx.Transactions.Remove( tr );
                                 ctx.SaveChanges();
                                 return Option<OperationViewModel>.Some( transaction );
                             }
                         }
                         else if ( ovm is TransferViewModel transfer )
                         {
                             var tr = ctx.Transfers.FirstOrDefault( x => x.OperationId == transfer.OperationId );
                             if ( tr != null )
                             {
                                 // Apply changes from editor
                                 new Mapper().Map( transfer , tr );
                                 ctx.Transfers.Remove( tr );
                                 ctx.SaveChanges();
                                 return Option<OperationViewModel>.Some( transfer );
                             }
                         }

                         return Option<OperationViewModel>.None;
                     } )
                     .None( () => Option<OperationViewModel>.None ) )
            );

            @this.PickCsvFileCommand = ReactiveCommand.CreateFromObservable( () =>
                @this.PickCsvFileInteraction.Handle( System.Reactive.Unit.Default ) , hasSelectedAccount );

            @this.ParseCsvContentCommand = ReactiveCommand.CreateFromObservable<string , OperationViewModel[]>( path =>
                Observable.Start( () =>
                {
                    return Array.Empty<OperationViewModel>();
                } ) );

            @this.ChooseCsvContentCommand = ReactiveCommand.CreateFromObservable<OperationViewModel[] , OperationViewModel[]>(
                input => @this.ChooseCsvContentInteraction.Handle( input )
            );
        }

        public static Func<OperationViewModel , bool> CreateSearchFilter( string searchText )
            => ovm =>
            {
                if ( string.IsNullOrWhiteSpace( searchText ) )
                    return true;

                var predicates = PredicatesBuilder.ParseAndBuild( searchText );
                return predicates.All( x => x( ovm ) );
            };
    }
}