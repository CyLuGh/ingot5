﻿using DynamicData;
using Ingots.Data;
using LanguageExt;
using Mapster;
using MoreLinq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.ViewModels
{
    public class IngotsViewModel : ReactiveObject, IActivatableViewModel
    {
        [Reactive] public UserSettings Settings { get; set; }
        [Reactive] public int SelectedIndex { get; set; } = 0;
        [Reactive] public string DatabaseFilePath { get; set; }
        [Reactive] public Option<IngotsContext> Context { get; set; }

        public SourceCache<AccountViewModel , int> AccountsCache { get; }
            = new SourceCache<AccountViewModel , int>( a => a.AccountId );

        public SourceCache<TransactionViewModel , (long, bool, Type)> TransactionsCache { get; }
            = new SourceCache<TransactionViewModel , (long, bool, Type)>( t => (t.OperationId, t.IsDerived, typeof( TransactionViewModel )) );

        public SourceCache<TransferViewModel , (long, bool, Type)> TransfersCache { get; }
            = new SourceCache<TransferViewModel , (long, bool, Type)>( t => (t.OperationId, t.IsDerived, typeof( TransferViewModel )) );

        public ReactiveCommand<string , System.Reactive.Unit> LoadContextCommand { get; private set; }

        public ReactiveCommand<System.Reactive.Unit , string> PickDbFileCommand { get; private set; }

        public Interaction<System.Reactive.Unit , string> PickDbFileInteraction { get; }
            = new Interaction<System.Reactive.Unit , string>( RxApp.MainThreadScheduler );

        public ViewModelActivator Activator { get; }
        public BankAccountsViewModel BankAccountsViewModel { get; }
        public ChartsViewModel ChartsViewModel { get; }
        public ReportsViewModel ReportsViewModel { get; }
        public HomeViewModel HomeViewModel { get; }

        public IngotsViewModel()
        {
            Activator = new ViewModelActivator();
            BankAccountsViewModel = new BankAccountsViewModel( this );
            ChartsViewModel = new ChartsViewModel( this );
            ReportsViewModel = new ReportsViewModel( this );
            HomeViewModel = new HomeViewModel( this );

            InitializeCommands( this );

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.Context )
                    .SubscribeSafe( x => x.ToObservable()
                       .SubscribeSafe( ctx =>
                       {
                           AccountsCache.Clear();
                           AccountsCache.AddOrUpdate( ctx.Accounts.ProjectToType<AccountViewModel>() );

                           TransactionsCache.Clear();
                           TransactionsCache.AddOrUpdate( ctx.Transactions.ProjectToType<TransactionViewModel>() );

                           TransfersCache.Clear();
                           TransfersCache.AddOrUpdate( ctx.Transfers.ProjectToType<TransferViewModel>() );
                       } )
                       .DisposeWith( disposables )
                       )
                    .DisposeWith( disposables );

                SetAccountsUpdaters( TransactionsCache , t => t.AccountId ).DisposeWith( disposables );
                SetAccountsUpdaters( TransfersCache , t => t.AccountId ).DisposeWith( disposables );
                SetAccountsUpdaters( TransfersCache , t => t.TargetAccountId , multiplier: -1 ).DisposeWith( disposables );

                PickDbFileCommand
                    .Where( path => !string.IsNullOrWhiteSpace( path ) )
                    .Do( path =>
                    {
                        DatabaseFilePath = path;
                        UserSettingsManager.Save( new UserSettings { DbPath = path } );
                    } )
                    .InvokeCommand( LoadContextCommand )
                    .DisposeWith( disposables );

                Observable.Merge(
                    PickDbFileCommand.ThrownExceptions.Select( ex => ("Couldn't pick db file", ex) ) ,
                    LoadContextCommand.ThrownExceptions.Select( ex => ("Couldn't load context", ex) )
                    )
                    .SubscribeSafe( t =>
                    {
                        var (title, exception) = t;
                        this.Log().Error( "{0}: {1}" , title , exception.Message );
                    } )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.Settings )
                    .Where( x => x == null || string.IsNullOrWhiteSpace( x.DbPath ) )
                    .Select( _ => Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ) , "ingots.db" ) )
                    .SubscribeSafe( path =>
                    {
                        DatabaseFilePath = path;
                        Context = Option<IngotsContext>.Some( new IngotsContext( path ) );
                    } )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.Settings )
                    .Where( x => x != null && !string.IsNullOrWhiteSpace( x.DbPath ) )
                    .Select( u => u.DbPath )
                    .SubscribeSafe( path =>
                    {
                        DatabaseFilePath = path;
                        Context = Option<IngotsContext>.Some( new IngotsContext( path ) );
                    } )
                    .DisposeWith( disposables );

                Observable.Return( System.Reactive.Unit.Default )
                    .Throttle( TimeSpan.FromMilliseconds( 20 ) )
                    .SubscribeSafe( _ =>
                    {
                        Settings = UserSettingsManager.Load();
                    } )
                    .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( IngotsViewModel @this )
        {
            @this.PickDbFileCommand = ReactiveCommand.CreateFromObservable( () =>
                @this.PickDbFileInteraction.Handle( System.Reactive.Unit.Default ) );

            @this.LoadContextCommand = ReactiveCommand.CreateFromObservable<string , System.Reactive.Unit>( path =>
                Observable.Start( () =>
                {
                    @this.Context.Some( ctx => ctx.Dispose() );
                    @this.Context = Option<IngotsContext>.Some( new IngotsContext( path ) );
                } ) );
        }

        private IDisposable SetAccountsUpdaters<T>( SourceCache<T , (long, bool, Type)> cache , Func<T , long> selector , int multiplier = 1 ) where T : OperationViewModel
            => cache.Connect()
                .AutoRefresh( x => x.IsExecuted )
                .SubscribeSafe( changes =>
                {
                    changes.Where( x => x.Reason == ChangeReason.Add ).Select( x => x.Current ).ForEach( change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault( o => o.AccountId == selector( change ) );
                        if ( account != null && change.IsExecuted )
                            account.OperationsValue += change.Value * multiplier;
                    } );

                    changes.Where( x => x.Reason == ChangeReason.Remove ).Select( x => x.Current ).ForEach( change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault( o => o.AccountId == selector( change ) );
                        if ( account != null && change.IsExecuted )
                            account.OperationsValue -= change.Value * multiplier;
                    } );

                    changes.Where( x => x.Reason == ChangeReason.Update ).ForEach( change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault( o => o.AccountId == selector( change.Previous.Value ) );
                        if ( account != null && change.Previous.Value.IsExecuted )
                            account.OperationsValue -= change.Previous.Value.Value * multiplier;
                        account = AccountsCache.Items.FirstOrDefault( o => o.AccountId == selector( change.Current ) );
                        if ( account != null && change.Current.IsExecuted )
                            account.OperationsValue += change.Current.Value * multiplier;
                    } );

                    changes.Where( x => x.Reason == ChangeReason.Refresh ).Select( x => x.Current ).ForEach( change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault( o => o.AccountId == selector( change ) );
                        if ( account != null )
                            account.OperationsValue += change.Value * ( change.IsExecuted ? 1 : -1 ) * multiplier;
                    } );
                } );
    }
}