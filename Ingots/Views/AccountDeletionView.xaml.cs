﻿using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    public partial class AccountDeletionView
    {
        public AccountDeletionView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(vm => PopulateFromViewModel(this, vm, disposables))
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private static void PopulateFromViewModel(AccountDeletionView view, AccountEditionViewModel viewModel, CompositeDisposable disposables)
        {
            view.OneWayBind(viewModel,
                vm => vm.Description,
                v => v.RunDescription.Text)
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                vm => vm.Bank,
                v => v.RunBank.Text)
                .DisposeWith(disposables);

            view.BindCommand(viewModel,
                vm => vm.CancelCommand,
                v => v.ButtonCancel)
                .DisposeWith(disposables);

            view.BindCommand(viewModel,
                vm => vm.ConfirmCommand,
                v => v.ButtonCreate)
                .DisposeWith(disposables);

            viewModel.CloseInteraction.RegisterHandler(ctx =>
            {
                DialogHost.CloseDialogCommand.Execute(ctx.Input, null);
                ctx.SetOutput(Unit.Default);
            })
            .DisposeWith(disposables);
        }
    }
}