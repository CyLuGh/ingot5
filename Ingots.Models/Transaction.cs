﻿namespace Ingots.Models
{
    public class Transaction : Operation
    {
        public string Category { get; set; } = string.Empty;
        public string SubCategory { get; set; } = string.Empty;
        public string Shop { get; set; } = string.Empty;

        public override string ToString()
            => $"TRANSACTION {Date:yyyy-MM-dd} {Description} {Value:N2}";
    }
}