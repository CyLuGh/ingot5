using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Linq;

namespace Ingots.ViewModels.Charts
{
    public class ColumnRectangleBarSeries : RectangleBarSeries
    {
        protected override object GetItem( int i )
        {
            if ( i < Items.Count && Items[i] is ColumnRectangleBarItem item )
                return item.Item;

            return base.GetItem( i );
        }

        public bool IsStacked { get; set; }
    }

    public class ColumnRectangleBarItem : RectangleBarItem
    {
        public IChartDataPoint Item { get; set; }

        public ColumnRectangleBarItem( double x0 , double y0 , double x1 , double y1 )
            : base( x0 , y0 , x1 , y1 )
        {
        }

        public ColumnRectangleBarItem( double x0 , double y0 , double x1 , double y1 , IChartDataPoint item )
            : this( x0 , y0 , x1 , y1 )
        {
            Item = item;
        }
    }

    public static class ColumnRectangleBarSeriesRender
    {
        public static void UpdateColumnBoundaries( this PlotModel plotModel , uint columnsCoefficient = 10 )
        {
            if ( plotModel.Series.Any( x => x is ColumnRectangleBarSeries ) )
            {
                var columnCount = plotModel.GetColumnCount();

                foreach ( var yAxisKey in plotModel.Series.OfType<ColumnRectangleBarSeries>().Select( x => x.YAxisKey ).Distinct() )
                {
                    uint i = 0;
                    foreach ( var rbs in plotModel.Series.OfType<ColumnRectangleBarSeries>().Where( x => x.YAxisKey.Equals( yAxisKey ) ) )
                    {
                        var bnd = GetColumnBoundaries( i , columnCount , columnsCoefficient );
                        foreach ( ColumnRectangleBarItem item in rbs.Items )
                        {
                            var columnInfo = item.Item as IChartDataPoint;

                            if ( columnInfo != null )
                            {
                                item.X0 = DateTimeAxis.ToDouble( columnInfo.Period.AddDays( bnd.Item1 ) );
                                item.X1 = DateTimeAxis.ToDouble( columnInfo.Period.AddDays( bnd.Item2 ) );

                                continue;
                            }

                            var stackedColumnInfo = item.Item as Tuple<DateTime , double , double>;
                            if ( stackedColumnInfo != null )
                            {
                                item.X0 = DateTimeAxis.ToDouble( stackedColumnInfo.Item1.AddDays( bnd.Item1 ) );
                                item.X1 = DateTimeAxis.ToDouble( stackedColumnInfo.Item1.AddDays( bnd.Item2 ) );

                                continue;
                            }
                        }

                        i++;
                    }
                }
            }
        }

        private static uint GetColumnCount( this PlotModel plotModel )
        {
            return (uint) ( plotModel.Series.OfType<ColumnRectangleBarSeries>().All( x => x.IsStacked ) ?
                1
                :
                plotModel.Series.OfType<ColumnRectangleBarSeries>().Where( x => !x.IsStacked ).GroupBy( x => x.YAxisKey ).Select( g => g.Count() ).Max() );
        }

        public static Tuple<int , int> GetColumnBoundaries( uint position , uint totalColumns , uint columnsCoefficient = 10 )
        {
            var boundaries = GetBoundaries( position , totalColumns );
            return Tuple.Create( (int) columnsCoefficient / 10 * boundaries.Item1 , (int) columnsCoefficient / 10 * boundaries.Item2 );
        }

        private static Tuple<int , int> GetBoundaries( uint position , uint totalColumns )
        {
            var boundaries = GetBoundaries( totalColumns );

            if ( position < boundaries.Length )
                return boundaries[position];

            return Tuple.Create( -10 , 10 );
        }

        private static Tuple<int , int>[] GetBoundaries( uint totalColumns )
        {
            switch ( totalColumns )
            {
                case 2:
                    return new[] { Tuple.Create( -10 , 0 ) , Tuple.Create( 0 , 10 ) };

                case 3:
                    return new[] { Tuple.Create( -9 , -3 ) , Tuple.Create( -3 , 3 ) , Tuple.Create( 3 , 9 ) };

                case 4:
                    return new[] { Tuple.Create( -10 , -5 ) , Tuple.Create( -5 , 0 ) , Tuple.Create( 0 , 5 ) , Tuple.Create( 5 , 10 ) };

                case 5:
                    return new[] { Tuple.Create( -10 , -6 ) , Tuple.Create( -6 , -2 ) , Tuple.Create( -2 , 2 ) , Tuple.Create( 2 , 6 ) , Tuple.Create( 6 , 10 ) };

                case 6:
                    return new[] { Tuple.Create( -9 , -6 ) , Tuple.Create( -6 , -3 ) , Tuple.Create( -3 , 0 ) , Tuple.Create( 0 , 3 ) , Tuple.Create( 3 , 6 ) , Tuple.Create( 6 , 9 ) };

                case 7:
                case 8:
                case 9:
                case 10:
                    return new[] { Tuple.Create( -10 , -8 ) , Tuple.Create( -8 , -6 ) , Tuple.Create( -6 , -4 ) , Tuple.Create( -4 , -2 ) , Tuple.Create( -2 , 0 ) , Tuple.Create( 0 , 2 ) , Tuple.Create( 2 , 4 ) , Tuple.Create( 4 , 6 ) , Tuple.Create( 6 , 8 ) , Tuple.Create( 8 , 10 ) };

                default:
                    return new[] { Tuple.Create( -10 , 10 ) };
            }
        }
    }
}