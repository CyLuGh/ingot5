﻿using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;

namespace Ingots.Views
{
    public partial class OperationEditionView
    {
        public OperationEditionView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( OperationEditionView view , OperationEditionViewModel viewModel , CompositeDisposable disposables )
        {
            view.ViewModelViewHost.ViewModel = viewModel;
            var operationType = viewModel is TransactionEditionViewModel ? "TRANSACTION" : "TRANSFER";
            var buttonText = viewModel.OperationId == 0 ? "CREATE" : "UPDATE";
            var headerText = viewModel.OperationId == 0 ? "NEW" : "EDIT";
            view.TextBlockHeader.Text = $"{headerText} {operationType}";
            view.ButtonCreate.Content = $"{buttonText} {operationType}";

            view.Bind( viewModel ,
                vm => vm.Date ,
                v => v.DatePicker.SelectedDate )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.IsExecuted ,
                v => v.CheckExecuted.IsChecked )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
               vm => vm.Description ,
               v => v.TextBoxDescription.Text )
               .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.ValueInput ,
                v => v.TextBoxValue.Text )
                .DisposeWith( disposables );

            view.TextBoxValue.Events().GotFocus
                .SubscribeSafe( _ => view.TextBoxValue.SelectAll() )
                .DisposeWith( disposables );

            view.TextBoxValue.Events().LostFocus
                .Select( _ => viewModel.Value.ToString( "C2" ) )
                .BindTo( view , x => x.TextBoxValue.Text )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.CancelCommand ,
                v => v.ButtonCancel )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.ConfirmCommand ,
                v => v.ButtonCreate )
                .DisposeWith( disposables );

            viewModel.CloseInteraction.RegisterHandler( ctx =>
            {
                DialogHost.CloseDialogCommand.Execute( ctx.Input , null );
                ctx.SetOutput( Unit.Default );
            } )
            .DisposeWith( disposables );
        }
    }
}