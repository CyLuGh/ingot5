﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ingots.Models
{
    public enum AccountKind
    {
        Checking = 0,
        Saving = 1,
        Locked = 2
    }
}