﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingots.ViewModels
{
    public class StashViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public string Name { get; set; }
        [Reactive] public AccountViewModel[] Accounts { get; set; }
        public double Value { [ObservableAsProperty] get; }

        public StashViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.Accounts )
                    .Where( x => x?.Any() == true )
                    .SubscribeSafe( accounts =>
                    {
                        accounts.Select( a => a.WhenAnyValue( x => x.StartValue , x => x.OperationsValue ).ToSignal() )
                            .Merge()
                            .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                            .Select( _ => accounts.Sum( a => a.StartValue + a.OperationsValue ) )
                            .ToPropertyEx( this , x => x.Value , scheduler: RxApp.MainThreadScheduler )
                            .DisposeWith( disposables );
                    } )
                    .DisposeWith( disposables );
            } );
        }
    }
}