﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    public partial class ImporterView
    {
        public ImporterView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .Subscribe()
                    .DisposeWith( disposables );
            } );
        }

        private void PopulateFromViewModel( ImporterView view , ImporterViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.NotSelectedOperations,
                v => v.ListBoxNotImported.ItemsSource)
                .DisposeWith(disposables);

            view.OneWayBind( viewModel ,
                vm => vm.SelectedOperations,
                v => v.ListBoxImported.ItemsSource)
                .DisposeWith(disposables);
        }
    }
}