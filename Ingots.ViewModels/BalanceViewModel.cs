﻿using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.ViewModels
{
    public class BalanceViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        [Reactive] public DateTime Period { get; set; }
        [Reactive] public double Income { get; set; }
        [Reactive] public double Expense { get; set; }
        [Reactive] public BalanceViewModel[] Components { get; set; }
        public double Balance { [ObservableAsProperty] get; }
        public double Average { [ObservableAsProperty] get; }
        public bool IsYearlyView { [ObservableAsProperty] get; }
        public string PeriodString { [ObservableAsProperty] get; }

        public BalanceViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.Components )
                    .Where( x => x?.Any() == true )
                    .SubscribeSafe( components =>
                    {
                        components.Select( c => c.WhenAnyValue( x => x.Income ) )
                            .Merge()
                            .ToSignal()
                            .SubscribeSafe( _ => Income = components.Sum( x => x.Income ) )
                            .DisposeWith( disposables );

                        components.Select( c => c.WhenAnyValue( x => x.Expense ) )
                            .Merge()
                            .ToSignal()
                            .SubscribeSafe( _ => Expense = components.Sum( x => x.Expense ) )
                            .DisposeWith( disposables );

                        components.Select( c => c.WhenAnyValue( x => x.Income ) )
                            .Merge()
                            .ToSignal()
                            .Merge( components.Select( c => c.WhenAnyValue( x => x.Expense ) )
                                .Merge()
                                .ToSignal() )
                            .Select( _ => components.Sum( x => x.Income - x.Expense ) / components.Length )
                            .ToPropertyEx( this , x => x.Average , 0d , scheduler: RxApp.MainThreadScheduler )
                            .DisposeWith( disposables );
                    } )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.Components )
                    .Select( x => x?.Any() == true )
                    .ToPropertyEx( this , x => x.IsYearlyView , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.Income )
                    .CombineLatest( this.WhenAnyValue( x => x.Expense ) )
                    .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                    .Select( t =>
                    {
                        var (income, expense) = t;
                        return income - expense;
                    } )
                    .ToPropertyEx( this , x => x.Balance , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.Period )
                    .CombineLatest( this.WhenAnyValue( x => x.IsYearlyView ) )
                    .Select( t =>
                    {
                        var (period, isYear) = t;
                        return isYear ? period.ToString( "yyyy" ) : period.ToString( "yyyy-MM" );
                    } )
                    .ToPropertyEx( this , x => x.PeriodString )
                    .DisposeWith( disposables );
            } );
        }
    }
}