﻿using Ingots.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Globalization;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.ViewModels
{
    public class AccountEditionViewModel : ReactiveObject, IActivatableViewModel
    {
        public AccountKind[] Kinds { get; } = Enum.GetValues<AccountKind>();

        public int AccountId { get; set; }
        [Reactive] public string Iban { get; set; }
        [Reactive] public string Description { get; set; }
        [Reactive] public string Bank { get; set; }
        [Reactive] public string Bic { get; set; }
        [Reactive] public string Stash { get; set; }
        [Reactive] public AccountKind Kind { get; set; }
        [Reactive] public string StartValueInput { get; set; }
        [Reactive] public bool IsDeleted { get; set; }

        public double StartValue { [ObservableAsProperty] get; }

        public ReactiveCommand<Unit, Unit> CancelCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> ConfirmCommand { get; private set; }

        public Interaction<bool, Unit> CloseInteraction { get; }
            = new Interaction<bool, Unit>(RxApp.MainThreadScheduler);

        public ViewModelActivator Activator { get; }

        public AccountEditionViewModel()
        {
            Activator = new ViewModelActivator();
            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.StartValueInput)
                    .Select(s =>
                    {
                        if (!string.IsNullOrWhiteSpace(s) && double.TryParse(s, NumberStyles.Currency, CultureInfo.CurrentCulture, out var start))
                            return start;

                        return 0d;
                    })
                    .ToPropertyEx(this, x => x.StartValue, scheduler: RxApp.MainThreadScheduler)
                    .DisposeWith(disposables);
            });
        }

        private static void InitializeCommands(AccountEditionViewModel @this)
        {
            @this.ConfirmCommand = ReactiveCommand.CreateFromObservable(() => @this.CloseInteraction.Handle(true));
            @this.CancelCommand = ReactiveCommand.CreateFromObservable(() => @this.CloseInteraction.Handle(false));
        }
    }
}