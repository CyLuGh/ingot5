﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Globalization;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.ViewModels
{
    public abstract class OperationEditionViewModel : ReactiveObject, IActivatableViewModel
    {
        [Reactive] public int AccountId { get; set; }
        [Reactive] public long OperationId { get; set; }
        [Reactive] public DateTime Date { get; set; }
        [Reactive] public string Description { get; set; }
        [Reactive] public bool IsExecuted { get; set; }
        [Reactive] public string ValueInput { get; set; }
        public double Value { [ObservableAsProperty] get; }

        public ReactiveCommand<Unit , Unit> CancelCommand { get; private set; }
        public ReactiveCommand<Unit , Unit> ConfirmCommand { get; private set; }

        public Interaction<bool , Unit> CloseInteraction { get; }
            = new Interaction<bool , Unit>( RxApp.MainThreadScheduler );

        public ViewModelActivator Activator { get; }

        protected OperationEditionViewModel()
        {
            Activator = new ViewModelActivator();
            InitializeCommands( this );

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( o => o.Date )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Subscribe( d => IsExecuted = d <= DateTime.Today )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.ValueInput )
                    .Select( s =>
                    {
                        if ( !string.IsNullOrWhiteSpace( s ) && double.TryParse( s , NumberStyles.Currency , CultureInfo.CurrentCulture , out var value ) )
                            return value;

                        return 0d;
                    } )
                    .ToPropertyEx( this , x => x.Value , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( OperationEditionViewModel @this )
        {
            @this.ConfirmCommand = ReactiveCommand.CreateFromObservable( () => @this.CloseInteraction.Handle( true ) );
            @this.CancelCommand = ReactiveCommand.CreateFromObservable( () => @this.CloseInteraction.Handle( false ) );
        }
    }
}