﻿using Ingots.ViewModels;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;

namespace Ingots.Views
{
    public partial class TransferDataGridIconView
    {
        public TransferDataGridIconView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( TransferDataGridIconView view , TransferViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.Value ,
                v => v.Icon.Foreground ,
                val => val < 0 ? Brushes.IndianRed : Brushes.SeaGreen )
            .DisposeWith( disposables );
        }
    }
}