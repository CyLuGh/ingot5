using Ingots.ViewModels.Charts;
using OxyPlot;
using OxyPlot.Axes;
using System.Windows.Media;

namespace Ingots.Views.Charts
{
    public static class OxyplotExtensions
    {
        private static OxyPlot.Wpf.OxyColorConverter ColorConverter { get; } = new OxyPlot.Wpf.OxyColorConverter();

        public static void Configure( this OxyPlot.Wpf.PlotView plotView , bool configureModel = true , bool hasLogarithmicAxis = false )
        {
            var bodyBrush = (Brush) plotView.TryFindResource( "MaterialDesignBody" ) ?? Brushes.DarkGray;
            var dividerBrush = (Brush) plotView.TryFindResource( "MaterialDesignDivider" ) ?? Brushes.Gray;

            var hoverController = new PlotController();
            hoverController.Bind( new OxyMouseEnterGesture() , PlotCommands.HoverTrack );
            plotView.Controller = hoverController;

            if ( configureModel )
            {
                ( plotView.Model ??= new PlotModel() ).Configure( bodyBrush , dividerBrush , hasLogarithmicAxis );
            }
        }

        public static void ConfigureCategories( this OxyPlot.Wpf.PlotView plotView )
        {
            var bodyBrush = (Brush) plotView.TryFindResource( "MaterialDesignBody" ) ?? Brushes.DarkGray;
            var dividerBrush = (Brush) plotView.TryFindResource( "MaterialDesignDivider" ) ?? Brushes.Gray;

            var hoverController = new PlotController();
            hoverController.Bind( new OxyMouseEnterGesture() , PlotCommands.HoverTrack );
            plotView.Controller = hoverController;

            var model = new PlotModel
            {
                PlotAreaBorderThickness = new OxyThickness( 0 )
            };
            model.AddAxis( new CategoryAxis() { Position = AxisPosition.Bottom , Key = "MAIN_X_AXIS" } , bodyBrush , dividerBrush );
            model.AddAxis( new LinearAxis { Position = AxisPosition.Left , Key = "MAIN_Y_AXIS" } , bodyBrush , dividerBrush );

            model.LegendPlacement = LegendPlacement.Outside;
            model.LegendPosition = LegendPosition.BottomCenter;
            model.LegendOrientation = LegendOrientation.Horizontal;
            model.IsLegendVisible = false;

            plotView.Model = model;
        }

        public static void Configure( this PlotModel model , Brush bodyBrush , Brush dividerBrush , bool hasLogarithmicAxis = false )
        {
            model.PlotAreaBorderThickness = new OxyThickness( 0 );
            model.LegendPlacement = LegendPlacement.Outside;
            model.LegendPosition = LegendPosition.BottomCenter;
            model.LegendOrientation = LegendOrientation.Horizontal;
            model.IsLegendVisible = false;

            model.AddAxis( new DateTimeAxis { Position = AxisPosition.Bottom , Key = "MAIN_X_AXIS" } , bodyBrush , dividerBrush );
            if ( hasLogarithmicAxis )
                model.AddAxis( new LogarithmicAxis { Position = AxisPosition.Left , Key = "MAIN_Y_AXIS" } , bodyBrush , dividerBrush );
            else
                model.AddAxis( new LinearAxis { Position = AxisPosition.Left , Key = "MAIN_Y_AXIS" } , bodyBrush , dividerBrush );
        }

        /// <summary>
        /// Configures and adds an axis to PlotModel
        /// </summary>
        public static void AddAxis( this PlotModel plotModel , Axis axis , Brush axisBrush , Brush gridBrush , LineStyle ls = LineStyle.Dot )
        {
            axis.ConfigureAxis( axisBrush , gridBrush , ls );
            plotModel.Axes.Add( axis );
        }

        /// <summary>
        /// Configures axis look
        /// </summary>
        public static void ConfigureAxis( this Axis axis , Brush axisBrush , Brush gridBrush , LineStyle ls = LineStyle.Dot )
        {
            axis.AxislineThickness = 1;
            axis.AxislineStyle = LineStyle.Solid;
            axis.AxislineColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
            axis.MinorTickSize = 0; // axis.ShowMinorTicks = false; being deprecated
            axis.TicklineColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
            axis.MajorGridlineStyle = ls;
            axis.MajorGridlineColor = (OxyColor) ColorConverter.ConvertBack( gridBrush , typeof( OxyColor ) , null , null );
            axis.TextColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
        }

        public static void AddStepLineSeries( this PlotModel plotModel , ChartSeries chartSeries , Brush brush )
        {
            var series = new OxyPlot.Series.StairStepSeries()
            {
                Title = chartSeries.Title ,
                Color = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerFill = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerStroke = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                ItemsSource = chartSeries ,
                DataFieldX = "Period" ,
                DataFieldY = "Value" ,
                CanTrackerInterpolatePoints = false ,
                Tag = chartSeries
            };

            plotModel.Series.Add( series );
        }

        public static void AddColumnSeries( this PlotModel plotModel , ChartSeries chartSeries , SolidColorBrush brush , uint position , uint totalColumns , uint columnsCoefficient = 10 )
        {
            var boundaries = ColumnRectangleBarSeriesRender.GetColumnBoundaries( position , totalColumns , columnsCoefficient );

            var fill = new SolidColorBrush( Color.FromArgb( 100 , brush.Color.R , brush.Color.G , brush.Color.B ) );

            var series = new ColumnRectangleBarSeries
            {
                Title = chartSeries.Title ,
                StrokeColor = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                FillColor = (OxyColor) ColorConverter.ConvertBack( fill , typeof( OxyColor ) , null , null ) ,
                TrackerKey = "ColumnTracker" ,
                IsStacked = false ,
                YAxisKey = "MAIN_Y_AXIS" ,
                Tag = chartSeries
            };

            foreach ( var v in chartSeries )
            {
                var item = new ColumnRectangleBarItem( DateTimeAxis.ToDouble( v.Period.AddDays( boundaries.Item1 ) ) , 0 , DateTimeAxis.ToDouble( v.Period.AddDays( boundaries.Item2 ) ) , v.Value , v );
                series.Items.Add( item );
            }

            plotModel.Series.Add( series );
        }
    }
}