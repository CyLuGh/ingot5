﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingots.ViewModels
{
    public class TransactionEditionViewModel : OperationEditionViewModel
    {
        [Reactive] public string Category { get; set; }
        [Reactive] public string SubCategory { get; set; }
        [Reactive] public string Shop { get; set; }

        [Reactive] public Dictionary<string , string[]> ExistingCategories { get; set; }
        [Reactive] public string[] ExistingShops { get; set; }

        public string[] Categories { [ObservableAsProperty]get; }
        public string[] SubCategories { [ObservableAsProperty]get; }
        public string[] Shops { [ObservableAsProperty]get; }

        public TransactionEditionViewModel() : base()
        {
            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ExistingCategories )
                    .Select( x => x.Keys.OrderBy( o => o ).ToArray() )
                    .ToPropertyEx( this , x => x.Categories , Array.Empty<string>() , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.ExistingCategories )
                    .CombineLatest( this.WhenAnyValue( x => x.Category ) )
                    .Select( tuple =>
                    {
                        var (dictionary, category) = tuple;
                        if ( dictionary != null && category != null && dictionary.TryGetValue( category , out var subcats ) )
                            return subcats.OrderBy( o => o ).ToArray();

                        return Array.Empty<string>();
                    } )
                    .ToPropertyEx( this , x => x.SubCategories , Array.Empty<string>() , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.ExistingShops )
                    .Select( o => o.OrderBy( x => x ).ToArray() )
                    .ToPropertyEx( this , x => x.Shops , Array.Empty<string>() , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );
            } );
        }
    }
}