﻿using Ingots.ViewModels;
using Ingots.Views.Charts;
using OxyPlot.Axes;
using ReactiveUI;
using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;

namespace Ingots.Views
{
    public partial class ChartsView
    {
        public ChartsView()
        {
            InitializeComponent();
            PlotViewYearly.Configure( hasLogarithmicAxis: false );
            ( PlotViewYearly.Model.Axes[0] as DateTimeAxis ).IntervalType = DateTimeIntervalType.Years;
            PlotViewMonthly.Configure( hasLogarithmicAxis: false );
            ( PlotViewMonthly.Model.Axes[0] as DateTimeAxis ).IntervalType = DateTimeIntervalType.Months;
            PlotViewEvolution.Configure();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( ChartsView view , ChartsViewModel viewModel , CompositeDisposable disposables )
        {
            viewModel.DrawMonthlyBalancesInteraction
                .RegisterHandler( ctx =>
                {
                    var (incomes, expenses) = ctx.Input;

                    view.PlotViewMonthly.Model.Series.Clear();
                    view.PlotViewMonthly.Model.AddColumnSeries( incomes , Brushes.ForestGreen , 0 , 2 );
                    view.PlotViewMonthly.Model.AddColumnSeries( expenses , Brushes.IndianRed , 1 , 2 );
                    view.PlotViewMonthly.InvalidatePlot( true );

                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            viewModel.DrawYearlyBalancesInteraction
                .RegisterHandler( ctx =>
                {
                    var (incomes, expenses) = ctx.Input;

                    view.PlotViewYearly.Model.Series.Clear();
                    view.PlotViewYearly.Model.AddColumnSeries( incomes , Brushes.ForestGreen , 0 , 2 , 100 );
                    view.PlotViewYearly.Model.AddColumnSeries( expenses , Brushes.IndianRed , 1 , 2 , 100 );
                    view.PlotViewYearly.InvalidatePlot( true );

                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            viewModel.DrawEvolutionSeriesInteraction
                .RegisterHandler( ctx =>
                {
                    view.PlotViewEvolution.Model.Series.Clear();
                    view.PlotViewEvolution.Model.AddStepLineSeries( ctx.Input , Brushes.CadetBlue );
                    view.PlotViewEvolution.InvalidatePlot( true );
                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );
        }
    }
}