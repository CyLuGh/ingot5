using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ingots.ViewModels.Charts
{
    public interface IChartDataPoint
    {
        DateTime Period { get; }
        double Value { get; }
    }

    public class ChartDataPoint : IChartDataPoint
    {
        public DateTime Period { get; set; }
        public double Value { get; set; }
    }

    public class ChartSeries : IEnumerable<IChartDataPoint>, ICloneable
    {
        public static Func<object , IEnumerable<IChartDataPoint>> DefaultDictionaryProvider { get; }
            = o => ( o as IDictionary<DateTime , double> )?.Select( x => new ChartDataPoint { Period = x.Key , Value = x.Value } );

        public string Title { get; set; }

        public Func<object , IEnumerable<IChartDataPoint>> DataPointsProvider { get; set; } = DefaultDictionaryProvider;
        public object DataSource { get; set; }

        public IEnumerable<IChartDataPoint> Data
            => DataSource != null && DataPointsProvider != null ? DataPointsProvider( DataSource ) : new List<ChartDataPoint>();

        public IEnumerator<IChartDataPoint> GetEnumerator()
        {
            foreach ( var e in Data.OrderBy( x => x.Period ) )
                yield return e;
        }

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        public object Clone()
            => MemberwiseClone();
    }
}