﻿using FileBrowser;
using Ingots.ViewModels;
using Ingots.Views;
using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using ReactiveUI;
using Splat;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;

namespace Ingots
{
    public partial class MainWindow : IViewFor<IngotsViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            ViewModel = Locator.Current.GetService<IngotsViewModel>();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( MainWindow view , IngotsViewModel viewModel , CompositeDisposable disposables )
        {
            view.HomeItem.Tag = viewModel.HomeViewModel;
            view.BankAccountsItem.Tag = viewModel.BankAccountsViewModel;
            view.ChartItem.Tag = viewModel.ChartsViewModel;
            view.ReportItem.Tag = viewModel.ReportsViewModel;

            view.Bind( viewModel ,
                vm => vm.SelectedIndex ,
                v => v.HamburgerMenu.SelectedIndex )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.DatabaseFilePath ,
                v => v.TextBlockFilePath.Text )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.PickDbFileCommand ,
                v => v.ButtonPickFile )
                .DisposeWith( disposables );

            viewModel.PickDbFileInteraction.RegisterHandler( ctx =>
            {
                //var picker = new FilePickerView() { Width = 1250 , Height = 800 };
                //picker.FileBrowser.ViewModel.FilterRegex = @"(\w)(?:\.db3|\.db)";
                //var res = (bool) await DialogHost.Show( picker ).ConfigureAwait( false );

                //if ( res )
                //    ctx.SetOutput( picker.FileBrowser.ViewModel.FileSelection );

                var dlg = new OpenFileDialog();
                if ( dlg.ShowDialog() == true )
                    ctx.SetOutput( dlg.FileName );
                else
                    ctx.SetOutput( string.Empty );
            } ).DisposeWith( disposables );
        }

        private void HamburgerMenu_OnItemInvoked( object sender , HamburgerMenuItemInvokedEventArgs args )
        {
            HamburgerMenu.Content = args.InvokedItem;
        }

        public IngotsViewModel ViewModel { get; set; }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (IngotsViewModel) value;
        }
    }
}