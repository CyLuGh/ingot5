﻿using Ingots.ViewModels;
using Ingots.Views;
using ReactiveUI;
using Splat;
using Splat.NLog;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;

namespace Ingots
{
    public partial class App : Application
    {
        public App()
        {
            FrameworkElement.LanguageProperty.OverrideMetadata( typeof( FrameworkElement ) , new FrameworkPropertyMetadata( XmlLanguage.GetLanguage( CultureInfo.CurrentCulture.IetfLanguageTag ) ) );
            FrameworkElement.LanguageProperty.OverrideMetadata( typeof( System.Windows.Documents.TextElement ) , new FrameworkPropertyMetadata( XmlLanguage.GetLanguage( CultureInfo.CurrentCulture.IetfLanguageTag ) ) );

            ConfigureLogs();
            Locator.CurrentMutable.UseNLogWithWrappingFullLogger();

            Locator.CurrentMutable.Register( () => new BankView() , typeof( IViewFor<BankViewModel> ) );
            Locator.CurrentMutable.Register( () => new AccountView() , typeof( IViewFor<AccountViewModel> ) );
            Locator.CurrentMutable.Register( () => new TransactionEditionView() , typeof( IViewFor<TransactionEditionViewModel> ) );
            Locator.CurrentMutable.Register( () => new TransferEditionView() , typeof( IViewFor<TransferEditionViewModel> ) );
            Locator.CurrentMutable.Register( () => new BalanceView() , typeof( IViewFor<BalanceViewModel> ) );
            Locator.CurrentMutable.Register( () => new StashView() , typeof( IViewFor<StashViewModel> ) );
            Locator.CurrentMutable.Register( () => new ImporterView() , typeof( IViewFor<ImporterViewModel> ) );

            Locator.CurrentMutable.Register( () => new HomeView() , typeof( IViewFor<HomeViewModel> ) , "MainWindow" );
            Locator.CurrentMutable.Register( () => new BankAccountsView() , typeof( IViewFor<BankAccountsViewModel> ) , "MainWindow" );
            Locator.CurrentMutable.Register( () => new ChartsView() , typeof( IViewFor<ChartsViewModel> ) , "MainWindow" );
            Locator.CurrentMutable.Register( () => new ReportsView() , typeof( IViewFor<ReportsViewModel> ) , "MainWindow" );

            Locator.CurrentMutable.Register( () => new TransactionDataGridView() , typeof( IViewFor<TransactionViewModel> ) , "OperationsGrid" );
            Locator.CurrentMutable.Register( () => new TransferDataGridView() , typeof( IViewFor<TransferViewModel> ) , "OperationsGrid" );
            Locator.CurrentMutable.Register( () => new TransactionDataGridIconView() , typeof( IViewFor<TransactionViewModel> ) , "OperationsGridIcon" );
            Locator.CurrentMutable.Register( () => new TransferDataGridIconView() , typeof( IViewFor<TransferViewModel> ) , "OperationsGridIcon" );

            Locator.CurrentMutable.RegisterConstant( new IngotsViewModel() , typeof( IngotsViewModel ) );
        }

        private static void ConfigureLogs()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget( "logfile" )
            {
                FileName = "ingots.log" ,
                DeleteOldFileOnStartup = true
            };
            var logconsole = new NLog.Targets.ConsoleTarget( "logconsole" );

            // Rules for mapping loggers to targets
            config.AddRule( NLog.LogLevel.Info , NLog.LogLevel.Fatal , logconsole );
            config.AddRule( NLog.LogLevel.Debug , NLog.LogLevel.Fatal , logfile );

            // Apply config
            NLog.LogManager.Configuration = config;
        }
    }
}