﻿using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.Views
{
    /// <summary>
    /// Interaction logic for OperationDeletionView.xaml
    /// </summary>
    public partial class OperationDeletionView
    {
        public OperationDeletionView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( OperationDeletionView view , OperationEditionViewModel viewModel , CompositeDisposable disposables )
        {
            var ivm = Locator.Current.GetService<IngotsViewModel>();

            view.RunKind.Text = viewModel is TransactionEditionViewModel ? "transaction" : "transfer";

            view.OneWayBind( viewModel ,
                vm => vm.Description ,
                v => v.RunDescription.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.AccountId ,
                v => v.RunAccount.Text ,
                id => ivm.AccountsCache.Items.FirstOrDefault( a => a.AccountId == id )?.Description ?? "?" )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.CancelCommand ,
                v => v.ButtonCancel )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.ConfirmCommand ,
                v => v.ButtonCreate )
                .DisposeWith( disposables );

            viewModel.CloseInteraction.RegisterHandler( ctx =>
            {
                DialogHost.CloseDialogCommand.Execute( ctx.Input , null );
                ctx.SetOutput( Unit.Default );
            } )
            .DisposeWith( disposables );
        }
    }
}