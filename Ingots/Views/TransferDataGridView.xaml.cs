﻿using Ingots.ViewModels;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Ingots.Views
{
    public partial class TransferDataGridView
    {
        public TransferDataGridView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( TransferDataGridView view , TransferViewModel viewModel , CompositeDisposable disposables )
        {
            var ivm = Locator.Current.GetService<IngotsViewModel>();

            view.OneWayBind( viewModel ,
                vm => vm.TargetAccountId ,
                v => v.TextBoxTarget.Text ,
                id => ivm.AccountsCache.Items.FirstOrDefault( a => a.AccountId == id)?.Description ?? "?"  )
            .DisposeWith( disposables );
        }
    }
}