﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    /// <summary>
    /// Interaction logic for HomeViewModel.xaml
    /// </summary>
    public partial class HomeView
    {
        public HomeView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( HomeView view , HomeViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.LastIncome ,
                v => v.TextBlockLastIncome.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.LastExpense ,
               v => v.TextBlockLastExpense.Text ,
               d => d.ToString( "C2" ) )
               .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.LastBalance ,
               v => v.TextBlockLastBalance.Text ,
               d => d.ToString( "C2" ) )
               .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.CurrentIncome ,
               v => v.TextBlockCurrentIncome.Text ,
               d => d.ToString( "C2" ) )
               .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.CurrentExpense ,
               v => v.TextBlockCurrentExpense.Text ,
               d => d.ToString( "C2" ) )
               .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.CurrentBalance ,
               v => v.TextBlockCurrentBalance.Text ,
               d => d.ToString( "C2" ) )
               .DisposeWith( disposables );
        }
    }
}