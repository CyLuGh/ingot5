﻿using Ingots.Models;
using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    public partial class AccountView
    {
        public AccountView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(x => PopulateFromViewModel(this, x, disposables))
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private static void PopulateFromViewModel(AccountView view, AccountViewModel viewModel, CompositeDisposable disposables)
        {
            view.OneWayBind(viewModel,
                v => v.Kind,
                vm => vm.KindIcon.Kind,
                ConvertAccountKindToPackIconKind)
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                v => v.Iban,
                vm => vm.IbanTextBlock.Text,
                IbanUtils.Format)
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                v => v.Description,
                vm => vm.DescriptionTextBlock.Text)
                .DisposeWith(disposables);

            view.OneWayBind(viewModel,
                v => v.CurrentValue,
                vm => vm.ValueTextBlock.Text,
                s => s.ToString("C2"))
                .DisposeWith(disposables);
        }

        private static PackIconKind ConvertAccountKindToPackIconKind( AccountKind kind ) => kind switch
        {
            AccountKind.Checking => PackIconKind.Money,
            AccountKind.Saving => PackIconKind.Bank,
            AccountKind.Locked => PackIconKind.Lock,
            _ => PackIconKind.QuestionMark,
        };
    }
}