﻿using DynamicData;
using Ingots.ViewModels;
using Ingots.Views.Charts;
using LanguageExt;
using Mapster;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;

namespace Ingots.Views
{
    public partial class BankAccountsView
    {
        public BankAccountsView()
        {
            InitializeComponent();
            PlotView.Configure();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( BankAccountsView view , BankAccountsViewModel viewModel , CompositeDisposable disposables )
        {
            view.BindCommand( viewModel ,
                vm => vm.CreateAccountCommand ,
                v => v.ButtonAddAccount )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.EditAccountCommand ,
                v => v.ButtonEditAccount ,
                viewModel.WhenAnyValue( x => x.SelectedAccount ) )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.DeleteAccountCommand ,
                v => v.ButtonDeleteAccount ,
                viewModel.WhenAnyValue( x => x.SelectedAccount ) )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.CreateTransactionCommand ,
                v => v.ButtonAddTransaction ,
                viewModel.WhenAnyValue( x => x.SelectedAccount ) )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.CreateTransferCommand ,
                v => v.ButtonAddTransfer ,
                viewModel.WhenAnyValue( x => x.SelectedAccount ) )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.EditOperationCommand ,
                v => v.ButtonEditOperation ,
                viewModel.WhenAnyValue( x => x.SelectedOperation ) )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.DeleteOperationCommand ,
                v => v.ButtonDeleteOperation ,
                viewModel.WhenAnyValue( x => x.SelectedOperation ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Banks ,
                v => v.ItemsControlBankAccounts.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.SelectedAccount ,
                v => v.TextBlockHeaderSelection.Text ,
                sa => sa != null ? sa.Description.ToUpperInvariant() : string.Empty )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Operations ,
                v => v.DataGridOperations.ItemsSource )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.SelectedOperation ,
                v => v.DataGridOperations.SelectedItem )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.SearchInput ,
                v => v.TextBoxSearch.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.PageParameters.CurrentPage ,
                v => v.RunCurrent.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.PageParameters.PageCount ,
                v => v.RunTotal.Text )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.PageParameters.PreviousPageCommand ,
                    v => v.ButtonPreviousPage )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.PageParameters.NextPageCommand ,
                    v => v.ButtonNextPage )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.PageParameters.FirstPageCommand ,
                    v => v.ButtonFirstPage )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.PageParameters.LastPageCommand ,
                    v => v.ButtonLastPage )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.PickCsvFileCommand ,
                v => v.ButtonImportCsv )
                .DisposeWith( disposables );

            viewModel.CreateAccountInteraction
                .RegisterHandler( async ctx =>
                {
                    var aevm = new AccountEditionViewModel();
                    var res = (bool) await DialogHost.Show( new AccountEditionView { ViewModel = aevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                        ctx.SetOutput( Option<AccountEditionViewModel>.Some( aevm ) );
                    else
                        ctx.SetOutput( Option<AccountEditionViewModel>.None );
                } )
                .DisposeWith( disposables );

            viewModel.EditAccountInteraction
                .RegisterHandler( async ctx =>
                {
                    TypeAdapterConfig<AccountViewModel , AccountEditionViewModel>.NewConfig()
                        .Map( dest => dest.StartValueInput , src => src.StartValue.ToString( "C2" ) );
                    var aevm = ctx.Input.Adapt<AccountEditionViewModel>();
                    var res = (bool) await DialogHost.Show( new AccountEditionView { ViewModel = aevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                        ctx.SetOutput( Option<AccountEditionViewModel>.Some( aevm ) );
                    else
                        ctx.SetOutput( Option<AccountEditionViewModel>.None );
                } )
                .DisposeWith( disposables );

            viewModel.DeleteAccountInteraction
                .RegisterHandler( async ctx =>
                {
                    TypeAdapterConfig<AccountViewModel , AccountEditionViewModel>.NewConfig()
                        .Map( dest => dest.StartValueInput , src => src.StartValue.ToString( "C2" ) );
                    var aevm = ctx.Input.Adapt<AccountEditionViewModel>();
                    var res = (bool) await DialogHost.Show( new AccountDeletionView { ViewModel = aevm , Width = 450 } ).ConfigureAwait( false );

                    if ( res )
                    {
                        aevm.IsDeleted = true;
                        ctx.SetOutput( Option<AccountEditionViewModel>.Some( aevm ) );
                    }
                    else
                    {
                        ctx.SetOutput( Option<AccountEditionViewModel>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.CreateTransactionInteraction
                .RegisterHandler( async ctx =>
                {
                    var tevm = ctx.Input;
                    var res = (bool) await DialogHost.Show( new OperationEditionView { ViewModel = tevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                    {
                        var tvm = tevm.Adapt<TransactionViewModel>();
                        ctx.SetOutput( Option<TransactionViewModel>.Some( tvm ) );
                    }
                    else
                    {
                        ctx.SetOutput( Option<TransactionViewModel>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.CreateTransferInteraction
                .RegisterHandler( async ctx =>
                {
                    var tevm = ctx.Input;
                    var res = (bool) await DialogHost.Show( new OperationEditionView { ViewModel = tevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                    {
                        TypeAdapterConfig<TransferEditionViewModel , TransferViewModel>.NewConfig()
                            .Map( dest => dest.TargetAccountId , src => src.SelectedTarget.AccountId );
                        var tvm = tevm.Adapt<TransferViewModel>();
                        ctx.SetOutput( Option<TransferViewModel>.Some( tvm ) );
                    }
                    else
                    {
                        ctx.SetOutput( Option<TransferViewModel>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.EditOperationInteraction
                .RegisterHandler( async ctx =>
                {
                    var tevm = ctx.Input;
                    var res = (bool) await DialogHost.Show( new OperationEditionView { ViewModel = tevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                    {
                        if ( tevm is TransactionEditionViewModel transaction )
                        {
                            ctx.SetOutput( Option<OperationViewModel>.Some( transaction.Adapt<TransactionViewModel>() ) );
                        }
                        else if ( tevm is TransferEditionViewModel transfer )
                        {
                            TypeAdapterConfig<TransferEditionViewModel , TransferViewModel>.NewConfig()
                                .Map( dest => dest.TargetAccountId , src => src.SelectedTarget.AccountId );
                            ctx.SetOutput( Option<OperationViewModel>.Some( transfer.Adapt<TransferViewModel>() ) );
                        }
                        else
                        {
                            ctx.SetOutput( Option<OperationViewModel>.None );
                        }
                    }
                    else
                    {
                        ctx.SetOutput( Option<OperationViewModel>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.DeleteOperationInteaction
                .RegisterHandler( async ctx =>
                {
                    var tevm = ctx.Input;
                    var res = (bool) await DialogHost.Show( new OperationDeletionView { ViewModel = tevm , Width = 650 } ).ConfigureAwait( false );

                    if ( res )
                    {
                        if ( tevm is TransactionEditionViewModel transaction )
                        {
                            ctx.SetOutput( Option<OperationViewModel>.Some( transaction.Adapt<TransactionViewModel>() ) );
                        }
                        else if ( tevm is TransferEditionViewModel transfer )
                        {
                            TypeAdapterConfig<TransferEditionViewModel , TransferViewModel>.NewConfig()
                                .Map( dest => dest.TargetAccountId , src => src.SelectedTarget.AccountId );
                            ctx.SetOutput( Option<OperationViewModel>.Some( transfer.Adapt<TransferViewModel>() ) );
                        }
                        else
                        {
                            ctx.SetOutput( Option<OperationViewModel>.None );
                        }
                    }
                    else
                    {
                        ctx.SetOutput( Option<OperationViewModel>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.DrawChartInteraction
                .RegisterHandler( ctx =>
                {
                    view.PlotView.Model.Series.Clear();
                    view.PlotView.Model.AddStepLineSeries( ctx.Input , Brushes.CadetBlue );
                    view.PlotView.InvalidatePlot( true );
                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            viewModel.PickCsvFileInteraction
                .RegisterHandler( ctx =>
                {
                    var dialog = new OpenFileDialog();

                    if ( dialog.ShowDialog() == true )
                    {
                        ctx.SetOutput( Option<string>.Some( dialog.FileName ) );
                    }
                    else
                    {
                        ctx.SetOutput( Option<string>.None );
                    }
                } )
                .DisposeWith( disposables );

            viewModel.ChooseCsvContentInteraction
                .RegisterHandler( async ctx =>
                {
                    var ivm = new ImporterViewModel();
                    ivm.RawImportCache.AddOrUpdate( ctx.Input );

                    await DialogHost.Show( new ImporterView { ViewModel = ivm , Width = 650 } ).ConfigureAwait( false );

                } )
                .DisposeWith( disposables );
        }
    }
}