﻿using LanguageExt;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingots.ViewModels
{
    public class TransferEditionViewModel : OperationEditionViewModel
    {
        [Reactive] public AccountViewModel[] Accounts { get; set; }
        [Reactive] public AccountViewModel SelectedTarget { get; set; }
        public AccountViewModel[] AvailableTargets { [ObservableAsProperty] get; }
        public Option<AccountViewModel> PreviousTarget { get; set; }

        public TransferEditionViewModel()
        {
            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.Accounts )
                    .CombineLatest( this.WhenAnyValue( x => x.AccountId ) )
                    .Select( t =>
                    {
                        var (accs, id) = t;
                        return accs.Where( a => a.AccountId != id )
                            .OrderBy( x => x.Bank )
                            .ThenBy( x => x.Description )
                            .ToArray();
                    } )
                    .ToPropertyEx( this , x => x.AvailableTargets , Array.Empty<AccountViewModel>() , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.AvailableTargets )
                    .WhereNotNull()
                    .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                    .SubscribeSafe( _ =>
                    {
                        PreviousTarget.ToObservable()
                            .ObserveOn( RxApp.MainThreadScheduler )
                            .SubscribeSafe( x => SelectedTarget = x );
                    } )
                    .DisposeWith( disposables );
            } );
        }
    }
}