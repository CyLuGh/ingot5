﻿using DynamicData;
using DynamicData.Binding;
using Ingots.ViewModels.Charts;
using MoreLinq;
using OxyPlot;
using OxyPlot.Series;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingots.ViewModels
{
    public class ReportsViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }
        public IngotsViewModel IngotsViewModel { get; }

        public double TotalAmount { [ObservableAsProperty] get; }
        public int AccountsCount { [ObservableAsProperty] get; }

        private ReadOnlyObservableCollection<BalanceViewModel> _balances;
        public ReadOnlyObservableCollection<BalanceViewModel> Balances => _balances;

        private ReadOnlyObservableCollection<StashViewModel> _stashes;
        public ReadOnlyObservableCollection<StashViewModel> Stashes => _stashes;

        public ReactiveCommand<TransactionViewModel[] , (ColumnSeries, string[])> BuildCategoriesColumnSeriesCommand { get; private set; }

        public Interaction<(ColumnSeries, string[]) , Unit> DisplayCategoriesColumnSeriesInteraction { get; }
            = new Interaction<(ColumnSeries, string[]) , Unit>( RxApp.MainThreadScheduler );

        public ReactiveCommand<TransactionViewModel[] , (ColumnSeries, string[])> BuildSubCategoriesColumnSeriesCommand { get; private set; }

        public Interaction<(ColumnSeries, string[]) , Unit> DisplaySubCategoriesColumnSeriesInteraction { get; }
            = new Interaction<(ColumnSeries, string[]) , Unit>( RxApp.MainThreadScheduler );

        public ReportsViewModel( IngotsViewModel ingotsViewModel )
        {
            Activator = new ViewModelActivator();
            IngotsViewModel = ingotsViewModel;
            InitializeCommands( this );

            DisplayCategoriesColumnSeriesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );
            DisplaySubCategoriesColumnSeriesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );

            BuildCategoriesColumnSeriesCommand.SubscribeSafe( async series =>
                await DisplayCategoriesColumnSeriesInteraction.Handle( series ) );

            BuildSubCategoriesColumnSeriesCommand.SubscribeSafe( async series =>
                await DisplaySubCategoriesColumnSeriesInteraction.Handle( series ) );

            this.WhenActivated( disposables =>
            {
                ingotsViewModel.AccountsCache.Connect()
                    .AutoRefresh( x => x.StartValue )
                    .ToSignal()
                    .CombineLatest( ingotsViewModel.TransactionsCache.Connect().AutoRefresh().ToSignal() )
                    .ObserveOn( RxApp.TaskpoolScheduler )
                    .Select( _ => ingotsViewModel.AccountsCache.Items.Sum( o => o.StartValue )
                        + ingotsViewModel.TransactionsCache.Items.Sum( o => o.Value ) )
                    .ToPropertyEx( this , x => x.TotalAmount , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                ingotsViewModel.AccountsCache.Connect()
                    .ToSignal()
                    .ObserveOn( RxApp.TaskpoolScheduler )
                    .Select( _ => ingotsViewModel.AccountsCache.Items.Count() )
                    .ToPropertyEx( this , x => x.AccountsCount , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache.Connect()
                    .ObserveOn( RxApp.TaskpoolScheduler )
                    .AutoRefresh( x => x.Value )
                    .AutoRefresh( x => x.IsExecuted )
                    .Group( tvm => new DateTime( tvm.Date.Year , tvm.Date.Month , 1 ) )
                    .Transform( g => new BalanceViewModel
                    {
                        Period = g.Key ,
                        Income = g.Cache.Items.Where( x => x.Value > 0 ).Sum( x => x.Value ) ,
                        Expense = g.Cache.Items.Where( x => x.Value < 0 ).Sum( x => Math.Abs( x.Value ) ) ,
                        Components = Array.Empty<BalanceViewModel>()
                    } )
                    .Sort( SortExpressionComparer<BalanceViewModel>.Descending( x => x.Period ) )
                    .Group( bvm => new DateTime( bvm.Period.Year , 1 , 1 ) )
                    .Transform( g => new BalanceViewModel
                    {
                        Period = g.Key ,
                        Components = g.Cache.Items.ToArray()
                    } )
                    .Sort( SortExpressionComparer<BalanceViewModel>.Descending( x => x.Period ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _balances )
                    .Subscribe()
                    .DisposeWith( disposables );

                ingotsViewModel.AccountsCache
                    .Connect()
                    .ObserveOn( RxApp.TaskpoolScheduler )
                    .AutoRefresh()
                    .Filter( a => !a.IsDeleted )
                    .Sort( SortExpressionComparer<AccountViewModel>.Ascending( x => x.Description ) )
                    .Group( avm => avm.Stash )
                    .Transform( g => new StashViewModel
                    {
                        Name = g.Key ,
                        Accounts = g.Cache.Items.ToArray()
                    } )
                    .Sort( SortExpressionComparer<StashViewModel>.Ascending( x => x.Name ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _stashes )
                    .Subscribe()
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh()
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .Select( _ => ingotsViewModel.TransactionsCache.Items.ToArray() )
                    .InvokeCommand( BuildCategoriesColumnSeriesCommand )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh()
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .Select( _ => ingotsViewModel.TransactionsCache.Items.ToArray() )
                    .InvokeCommand( BuildSubCategoriesColumnSeriesCommand )
                    .DisposeWith( disposables );

                Observable.Merge(
                    BuildCategoriesColumnSeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build categories", ex) ) ,
                    BuildSubCategoriesColumnSeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build subcategories", ex) )
                )
                .SubscribeSafe( t =>
                {
                    var (title, exception) = t;
                    this.Log().Error( "{0} > {1}" , title , exception.Message );
                } )
                .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( ReportsViewModel @this )
        {
            // @see https://coolors.co/f94144-f3722c-f8961e-f9c74f-90be6d-43aa8b-577590
            var colors = new[]
            {
                OxyColor.FromArgb(255, 249, 65, 68),
                OxyColor.FromArgb(255, 243, 114, 44),
                OxyColor.FromArgb(255, 248, 150, 30),
                OxyColor.FromArgb(255, 249, 199, 79),
                OxyColor.FromArgb(255, 144, 190, 109),
                OxyColor.FromArgb(255, 67, 170, 139),
                OxyColor.FromArgb(255, 87, 117, 144)
            };

            @this.BuildCategoriesColumnSeriesCommand = ReactiveCommand.CreateFromObservable<TransactionViewModel[] , (ColumnSeries, string[])>( transactions =>
                Observable.Start( () =>
                {
                    var categories = transactions
                        .Where( t => t.IsExecuted && t.Value < 0 )
                        .GroupBy( t => t.Category )
                        .Select( g => (Category: g.Key, Value: Math.Abs( g.Sum( o => o.Value ) )) )
                        .OrderByDescending( t => t.Value )
                        .Take( 7 )
                        .ToArray();

                    var series = new ColumnSeries();
                    categories.ForEach( ( c , i ) => series.Items.Add( new ExtendedColumnItem
                    {
                        Category = c.Category ,
                        Value = c.Value ,
                        Color = colors[i]
                    } ) );
                    return (series, categories.Select( c => c.Category ).ToArray());
                } ) );

            @this.BuildSubCategoriesColumnSeriesCommand = ReactiveCommand.CreateFromObservable<TransactionViewModel[] , (ColumnSeries, string[])>( transactions =>
                Observable.Start( () =>
                {
                    var categories = transactions
                        .Where( t => t.IsExecuted && t.Value < 0 )
                        .GroupBy( t => (t.Category, t.SubCategory) )
                        .Select( g => (Category: g.Key, Value: Math.Abs( g.Sum( o => o.Value ) )) )
                        .OrderByDescending( t => t.Value )
                        .Take( 7 )
                        .ToArray();

                    var series = new ColumnSeries();
                    categories.ForEach( ( c , i ) => series.Items.Add( new ExtendedColumnItem
                    {
                        Category = $"{c.Category.Category} / {c.Category.SubCategory}" ,
                        Value = c.Value ,
                        Color = colors[i]
                    } ) );
                    return (series, categories.Select( c => $"{c.Category.Category} / {c.Category.SubCategory}" ).ToArray());
                } ) );
        }
    }
}