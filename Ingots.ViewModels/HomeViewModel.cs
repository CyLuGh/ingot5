﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingots.ViewModels
{
    public class HomeViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public double LastIncome { [ObservableAsProperty] get; }
        public double LastExpense { [ObservableAsProperty] get; }
        public double LastBalance { [ObservableAsProperty] get; }

        public double CurrentIncome { [ObservableAsProperty] get; }
        public double CurrentExpense { [ObservableAsProperty] get; }
        public double CurrentBalance { [ObservableAsProperty] get; }

        public HomeViewModel( IngotsViewModel ingotsViewModel )
        {
            Activator = new ViewModelActivator();

            this.WhenActivated( disposables =>
            {
                ingotsViewModel.TransactionsCache
                    .Connect()
                    .ToSignal()
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( _ =>
                    {
                        var id = DateTime.Today.AddMonths( -1 ).ToId();
                        return ingotsViewModel.TransactionsCache.Items.Where( x => x.Date.ToId() == id && x.Value > 0 )
                            .Sum( x => x.Value );
                    } )
                    .ToPropertyEx( this , x => x.LastIncome , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .ToSignal()
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( _ =>
                    {
                        var id = DateTime.Today.AddMonths( -1 ).ToId();
                        return ingotsViewModel.TransactionsCache.Items.Where( x => x.Date.ToId() == id && x.Value < 0 )
                            .Sum( x => Math.Abs( x.Value ) );
                    } )
                    .ToPropertyEx( this , x => x.LastExpense , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.LastIncome )
                    .CombineLatest( this.WhenAnyValue( x => x.LastExpense ) )
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( t =>
                    {
                        var (income, expense) = t;
                        return income - expense;
                    } )
                    .ToPropertyEx( this , x => x.LastBalance , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .ToSignal()
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( _ =>
                    {
                        var id = DateTime.Today.ToId();
                        return ingotsViewModel.TransactionsCache.Items.Where( x => x.Date.ToId() == id && x.Value > 0 )
                            .Sum( x => x.Value );
                    } )
                    .ToPropertyEx( this , x => x.CurrentIncome , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .ToSignal()
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( _ =>
                    {
                        var id = DateTime.Today.ToId();
                        return ingotsViewModel.TransactionsCache.Items.Where( x => x.Date.ToId() == id && x.Value < 0 )
                            .Sum( x => Math.Abs( x.Value ) );
                    } )
                    .ToPropertyEx( this , x => x.CurrentExpense , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.CurrentIncome )
                    .CombineLatest( this.WhenAnyValue( x => x.CurrentExpense ) )
                    .Throttle( TimeSpan.FromMilliseconds( 150 ) )
                    .Select( t =>
                    {
                        var (income, expense) = t;
                        return income - expense;
                    } )
                    .ToPropertyEx( this , x => x.CurrentBalance , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );
            } );
        }
    }

    internal static class DateTimeExtensions
    {
        internal static int ToId( this DateTime dt )
            => ( dt.Year * 12 ) + dt.Month - 1;
    }
}