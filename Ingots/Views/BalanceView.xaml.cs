﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    /// <summary>
    /// Interaction logic for BalanceView.xaml
    /// </summary>
    public partial class BalanceView
    {
        public BalanceView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( BalanceView view , BalanceViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.PeriodString ,
                v => v.TextBlockPeriod.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Income ,
                v => v.TextBlockIncome.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Expense ,
                v => v.TextBlockExpense.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Balance ,
                v => v.PackIconBalance.Foreground ,
                d => d < 0 ? Brushes.IndianRed : Brushes.SeaGreen )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Balance ,
                v => v.TextBlockBalance.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Average ,
                v => v.TextBlockAverage.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.IsYearlyView ,
                v => v.StackPanelAverage.Visibility ,
                b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );
        }
    }
}