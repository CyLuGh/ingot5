﻿using Ingots.ViewModels;
using Ingots.Views.Charts;
using OxyPlot.Axes;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    /// <summary>
    /// Interaction logic for ReportsView.xaml
    /// </summary>
    public partial class ReportsView
    {
        public ReportsView()
        {
            InitializeComponent();
            PlotViewCategories.ConfigureCategories();
            PlotViewSubCategories.ConfigureCategories();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( ReportsView view , ReportsViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.TotalAmount ,
                v => v.RunTotalAmount.Text ,
                a => a.ToString( "C2" ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
               vm => vm.AccountsCount ,
               v => v.RunAccountsCount.Text )
               .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Stashes ,
                v => v.TreeViewStashes.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Balances ,
                v => v.TreeViewBalances.ItemsSource )
                .DisposeWith( disposables );

            viewModel.DisplayCategoriesColumnSeriesInteraction
                .RegisterHandler( ctx =>
                {
                    view.PlotViewCategories.Model.Series.Clear();
                    ((CategoryAxis) view.PlotViewCategories.Model.Axes[0]).ItemsSource = ctx.Input.Item2;
                    view.PlotViewCategories.Model.Series.Add(ctx.Input.Item1);
                    view.PlotViewCategories.InvalidatePlot();

                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            viewModel.DisplaySubCategoriesColumnSeriesInteraction
                .RegisterHandler( ctx =>
                {
                    view.PlotViewSubCategories.Model.Series.Clear();
                    ((CategoryAxis) view.PlotViewSubCategories.Model.Axes[0]).ItemsSource = ctx.Input.Item2;
                    view.PlotViewSubCategories.Model.Series.Add(ctx.Input.Item1);
                    view.PlotViewSubCategories.InvalidatePlot();

                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );
        }
    }
}