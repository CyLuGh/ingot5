﻿using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;

namespace Ingots.Views
{
    public partial class AccountEditionView
    {
        public AccountEditionView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( AccountEditionView view , AccountEditionViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.AccountId ,
                v => v.TextBlockHeader.Text ,
                id => id == 0 ? "NEW ACCOUNT" : "EDIT ACCOUNT" )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.AccountId ,
                v => v.ButtonCreate.Content ,
                id => id == 0 ? "CREATE ACCOUNT" : "UPDATE ACCOUNT" )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Iban ,
                v => v.TextBoxIban.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Description ,
                v => v.TextBoxDescription.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Bank ,
                v => v.TextBoxBank.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Bic ,
                v => v.TextBoxBic.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Stash ,
                v => v.TextBoxStash.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Kinds ,
                v => v.ComboBoxKind.ItemsSource )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.Kind ,
                v => v.ComboBoxKind.SelectedItem )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.CancelCommand ,
                v => v.ButtonCancel )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                vm => vm.ConfirmCommand ,
                v => v.ButtonCreate )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.StartValueInput ,
                v => v.TextBoxStartValue.Text )
                .DisposeWith( disposables );

            view.TextBoxStartValue.Events().GotFocus
                .SubscribeSafe( _ => view.TextBoxStartValue.SelectAll() )
                .DisposeWith( disposables );

            view.TextBoxStartValue.Events().LostFocus
                .Select( _ => viewModel.StartValue.ToString( "C2" ) )
                .BindTo( view , x => x.TextBoxStartValue.Text )
                .DisposeWith( disposables );

            viewModel.CloseInteraction.RegisterHandler( ctx =>
             {
                 DialogHost.CloseDialogCommand.Execute( ctx.Input , null );
                 ctx.SetOutput( Unit.Default );
             } )
            .DisposeWith( disposables );
        }
    }
}