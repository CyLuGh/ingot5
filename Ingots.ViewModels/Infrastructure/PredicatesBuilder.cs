using System;
using System.Linq;

namespace Ingots.ViewModels.Infrastructure
{
    public static class PredicatesBuilder
    {
        public static Predicate<OperationViewModel>[] ParseAndBuild(string input, bool checkContent = true)
            => input.Split('&').Select(s => BuildPredicate(s, checkContent)).ToArray();

        private static Predicate<OperationViewModel> BuildPredicate(string input, bool checkContent = true)
        {
            var searched = input.Trim();

            if (searched.StartsWith("<=") && double.TryParse(searched.Substring(2), out var val))
                return x => x.Value <= val;

            if (searched.StartsWith(">=") && double.TryParse(searched.Substring(2), out val))
                return x => x.Value >= val;

            if (searched.StartsWith("<") && double.TryParse(searched.Substring(1), out val))
                return x => x.Value < val;
            if (searched.StartsWith(">") && double.TryParse(searched.Substring(1), out val))
                return x => x.Value > val;

            if (searched.StartsWith("<=") && DateTime.TryParse(searched.Substring(2), out var date))
                return x => x.Date <= date;
            if (searched.StartsWith(">=") && DateTime.TryParse(searched.Substring(2), out date))
                return x => x.Date >= date;

            if (searched.StartsWith("<") && DateTime.TryParse(searched.Substring(1), out date))
                return x => x.Date < date;
            if (searched.StartsWith(">") && DateTime.TryParse(searched.Substring(1), out date))
                return x => x.Date > date;

            if (int.TryParse(searched, out int year))
                return x => x.Date.Year == year;

            if (searched.Split('-').Length == 2)
            {
                var split = searched.Split('-');
                if (int.TryParse(split[0], out year) && int.TryParse(split[1], out int month))
                    return x => x.Date.Year == year && x.Date.Month == month;
            }

            if (DateTime.TryParse(searched, out var dt))
                return x => x.Date.Equals(dt);

            if ( checkContent )
            {
                return x => x.Description?.StartsWith( searched , StringComparison.CurrentCultureIgnoreCase ) == true
                           || ( x as TransactionViewModel )?.Category.StartsWith( searched , StringComparison.CurrentCultureIgnoreCase ) == true
                           || ( x as TransactionViewModel )?.SubCategory.StartsWith( searched , StringComparison.CurrentCultureIgnoreCase ) == true
                           || ( x as TransactionViewModel )?.Shop.StartsWith( searched , StringComparison.CurrentCultureIgnoreCase ) == true;
            }
            else
            {
                return _ => true;
            }
        }
    }

}