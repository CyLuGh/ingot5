﻿using Ingots.Data;
using Ingots.Models;
using Ingots.ViewModels.CsvManagement;

internal class Program
{
    private static void Main( string[] args )
    {
        var dbPath = args[0];
        var bank = args[1];
        var iban = args[2];
        var csvFile = args[3];

        if ( File.Exists( dbPath ) )
        {
            var context = new IngotsContext( dbPath );
            var accounts = context.Accounts;

            var account = accounts.Find( a => a.Iban.Equals( iban ) );

            account.Match( a =>
            {
                var operations = CsvImporter.ParseFile( csvFile , a , accounts.ToArray() /*, new DateTime( 2019 , 1 , 1 )*/ ).ToArray();
                var transactions = operations.OfType<Transaction>().ToArray();
                var transfers = operations.OfType<Transfer>().ToArray();

                Console.WriteLine( $"{transactions.Length} transactions & {transfers.Length} transfers" );

                var stored = new List<Operation>();
                stored.AddRange( context.Transactions.Where( t => t.AccountId == a.AccountId ) );
                stored.AddRange( context.Transfers.Where( t => t.AccountId == a.AccountId ) );

                var common = stored.Join( operations ,
                    s => s ,
                    t => t ,
                    ( s , t ) => (s, t) ,
                    new FuzzyOperationComparer() )
                .ToArray();

                foreach ( var c in common )
                {
                    var (s, t) = c;
                    if ( s.Date != t.Date )
                        Console.WriteLine( $"In db:{s} In csv: {t}" );
                }
            } , () => { } );
        }
    }
}