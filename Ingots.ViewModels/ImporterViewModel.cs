﻿using System.Collections.ObjectModel;
using DynamicData;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Disposables;

namespace Ingots.ViewModels
{
    public class ImporterViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public SourceCache<OperationViewModel , string> RawImportCache { get; }
            = new SourceCache<OperationViewModel , string>( x => x.ImportReference );

        private ReadOnlyObservableCollection<OperationViewModel> _notSelectedOperations;
        public ReadOnlyObservableCollection<OperationViewModel> NotSelectedOperations => _notSelectedOperations;

        private ReadOnlyObservableCollection<OperationViewModel> _selectedOperations;
        public ReadOnlyObservableCollection<OperationViewModel> SelectedOperations => _selectedOperations;

        public ImporterViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenActivated( disposables =>
            {
                RawImportCache.Connect()
                    .AutoRefresh( x => x.IsSelectedForImport )
                    .Filter( x => !x.IsSelectedForImport )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _notSelectedOperations )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                RawImportCache.Connect()
                    .AutoRefresh( x => x.IsSelectedForImport )
                    .Filter( x => x.IsSelectedForImport )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _selectedOperations )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );
            } );
        }
    }
}