﻿using FileBrowser;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    public partial class FilePickerView
    {
        public FilePickerView()
        {
            InitializeComponent();

            FileBrowser.ViewModel.Mode = BrowserMode.Create;

            ButtonCancel.Click += ( sender , args ) => DialogHost.CloseDialogCommand.Execute( false , null );
            ButtonSelect.Click += ( sender , args ) => DialogHost.CloseDialogCommand.Execute( true , null );
        }
    }
}