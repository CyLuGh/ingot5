using OxyPlot;
using System;
using System.Windows.Data;

namespace Ingots.Views.Charts
{
    public class TrackerConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert( object value , Type targetType , object parameter , System.Globalization.CultureInfo culture )
        {
            switch ( parameter.ToString() )
            {
                case "Period":
                {
                    if ( value is double d )
                    {
                        return OxyPlot.Axes.DateTimeAxis.ToDateTime( d );
                    }
                    if ( value is DateTime dateTime )
                    {
                        return dateTime;
                    }
                }
                break;

                case "Foreground":
                {
                    if ( value is OxyColor oxyColor )
                    {
                        return PerceivedBrightness( oxyColor ) > 130 ? System.Windows.Media.Brushes.Black : System.Windows.Media.Brushes.White;
                    }
                }
                break;

                case "TrackerLine":
                {
                    if ( value is bool b )
                        return b ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                }
                break;
            }

            return null;
        }

        private static int PerceivedBrightness( OxyColor c )
            => (int) Math.Sqrt(
                ( c.R * c.R * .299 ) +
                ( c.G * c.G * .587 ) +
                ( c.B * c.B * .114 )
            );

        public object ConvertBack( object value , Type targetType , object parameter , System.Globalization.CultureInfo culture )
        {
            return null;
        }

        #endregion IValueConverter Members
    }
}