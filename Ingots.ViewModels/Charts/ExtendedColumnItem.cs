﻿using OxyPlot.Series;

namespace Ingots.ViewModels.Charts
{
    public class ExtendedColumnItem : ColumnItem
    {
        public string Category { get; set; }
    }
}