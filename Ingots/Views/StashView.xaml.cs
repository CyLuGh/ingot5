﻿using Ingots.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ingots.Views
{
    public partial class StashView
    {
        public StashView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables ) );
        }

        private static void PopulateFromViewModel( StashView view , StashViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.Name ,
                v => v.TextBlockName.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Value ,
                v => v.TextBlockValue.Text ,
                d => d.ToString( "C2" ) )
                .DisposeWith( disposables );
        }
    }
}
