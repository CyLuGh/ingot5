using DynamicData.Binding;
using DynamicData.Operators;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive;
using System.Reactive.Linq;

namespace Ingots.ViewModels.Infrastructure
{
    public class PageParameterData : ReactiveObject
    {
        public PageParameterData( int currentPage , int pageSize )
        {
            CurrentPage = currentPage;
            PageSize = pageSize;

            NextPageCommand = ReactiveCommand.Create( () => { CurrentPage += 1; } ,
                this.WhenChanged( x => x.CurrentPage , x => x.PageCount , ( _ , cp , pc ) => cp < pc ) );

            PreviousPageCommand = ReactiveCommand.Create( () => { CurrentPage -= 1; } ,
                this.WhenAnyValue( x => x.CurrentPage ).Select( cp => cp > 1 ) );

            FirstPageCommand = ReactiveCommand.Create( () => { CurrentPage = 1; } ,
                this.WhenAnyValue( x => x.CurrentPage ).Select( cp => cp > 1 ) );

            LastPageCommand = ReactiveCommand.Create( () => { CurrentPage = PageCount; } ,
                this.WhenChanged( x => x.CurrentPage , x => x.PageCount , ( _ , cp , pc ) => cp < pc ) );
        }

        public void Update( IPageResponse response )
        {
            CurrentPage = response.Page;
            PageSize = response.PageSize;
            PageCount = response.Pages;
            TotalCount = response.TotalSize;
        }

        [Reactive] public int CurrentPage { get; set; }
        [Reactive] public int PageCount { get; set; }
        [Reactive] public int PageSize { get; set; }
        [Reactive] public int TotalCount { get; set; }

        public ReactiveCommand<Unit , Unit> NextPageCommand { get; }
        public ReactiveCommand<Unit , Unit> PreviousPageCommand { get; }
        public ReactiveCommand<Unit , Unit> FirstPageCommand { get; }
        public ReactiveCommand<Unit , Unit> LastPageCommand { get; }
    }

}