﻿using DynamicData;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Collections.Generic;
using Ingots.ViewModels.Charts;

namespace Ingots.ViewModels
{
    public class ChartsViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public ReactiveCommand<IChangeSet<(DateTime Period, double Income, double Expense) , DateTime> , (ChartSeries, ChartSeries)> BuildMonthlySeriesCommand { get; private set; }
        public ReactiveCommand<IChangeSet<(DateTime Period, double Income, double Expense) , DateTime> , (ChartSeries, ChartSeries)> BuildYearlySeriesCommand { get; private set; }
        public ReactiveCommand<(double, TransactionViewModel[]) , ChartSeries> BuildEvolutionsSeriesCommand { get; private set; }

        public Interaction<(ChartSeries, ChartSeries) , Unit> DrawMonthlyBalancesInteraction { get; }
            = new Interaction<(ChartSeries, ChartSeries) , Unit>( RxApp.MainThreadScheduler );

        public Interaction<(ChartSeries, ChartSeries) , Unit> DrawYearlyBalancesInteraction { get; }
            = new Interaction<(ChartSeries, ChartSeries) , Unit>( RxApp.MainThreadScheduler );

        public Interaction<ChartSeries , Unit> DrawEvolutionSeriesInteraction { get; }
            = new Interaction<ChartSeries , Unit>( RxApp.MainThreadScheduler );

        public ChartsViewModel( IngotsViewModel ingotsViewModel )
        {
            Activator = new ViewModelActivator();
            InitializeCommands( this );

            DrawMonthlyBalancesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );
            DrawYearlyBalancesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );
            DrawEvolutionSeriesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );

            BuildMonthlySeriesCommand.SubscribeSafe( async series => await DrawMonthlyBalancesInteraction.Handle( series ) );
            BuildYearlySeriesCommand.SubscribeSafe( async series => await DrawYearlyBalancesInteraction.Handle( series ) );
            BuildEvolutionsSeriesCommand.SubscribeSafe( async series => await DrawEvolutionSeriesInteraction.Handle( series ) );

            this.WhenActivated( disposables =>
            {
                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh( t => t.IsExecuted )
                    .Group( t => new DateTime( t.Date.Year , t.Date.Month , 1 ) )
                    .Transform( g => (Period: g.Key,
                        Income: g.Cache.Items.Where( t => t.Value > 0 ).Sum( t => t.Value ),
                        Expense: g.Cache.Items.Where( t => t.Value < 0 ).Sum( t => Math.Abs( t.Value ) )) )
                    .InvokeCommand( BuildMonthlySeriesCommand )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh( t => t.IsExecuted )
                    .Group( t => new DateTime( t.Date.Year , 1 , 1 ) )
                    .Transform( g => (Period: g.Key,
                        Income: g.Cache.Items.Where( t => t.Value > 0 ).Sum( t => t.Value ),
                        Expense: g.Cache.Items.Where( t => t.Value < 0 ).Sum( t => Math.Abs( t.Value ) )) )
                    .InvokeCommand( BuildYearlySeriesCommand )
                    .DisposeWith( disposables );

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh()
                    .CombineLatest( ingotsViewModel.AccountsCache.Connect().WhenValueChanged( x => x.StartValue ) ,
                    ( _ , __ ) => (ingotsViewModel.AccountsCache.Items.Sum( o => o.StartValue ), ingotsViewModel.TransactionsCache.Items.ToArray()) )
                    .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                    .InvokeCommand( BuildEvolutionsSeriesCommand )
                    .DisposeWith( disposables );

                Observable.Merge(
                    BuildMonthlySeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build monthly series", ex) ) ,
                    BuildYearlySeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build yearly series", ex) ) ,
                    BuildEvolutionsSeriesCommand.ThrownExceptions.Select( ex => ("Couldn't build evolution series", ex) )
                    )
                    .SubscribeSafe( t =>
                    {
                        var (title, exception) = t;
                        this.Log().Error( "{0}: {1}" , title , exception.Message );
                    } )
                    .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( ChartsViewModel @this )
        {
            @this.BuildMonthlySeriesCommand = ReactiveCommand
                .CreateFromObservable<IChangeSet<(DateTime Period, double Income, double Expense) , DateTime> , (ChartSeries, ChartSeries)>( changes =>
                      Observable.Start( () =>
                      {
                          var incomes = changes.Where( x => x.Reason == ChangeReason.Add )
                              .Select( x => x.Current )
                              .ToDictionary( x => x.Period , x => x.Income );
                          var expenses = changes.Where( x => x.Reason == ChangeReason.Add )
                              .Select( x => x.Current )
                              .ToDictionary( x => x.Period , x => x.Expense );

                          return (new ChartSeries
                          {
                              Title = "Income" ,
                              DataSource = incomes
                          },
                          new ChartSeries
                          {
                              Title = "Expense" ,
                              DataSource = expenses
                          });
                      } ) );

            @this.BuildYearlySeriesCommand = ReactiveCommand
                .CreateFromObservable<IChangeSet<(DateTime Period, double Income, double Expense) , DateTime> , (ChartSeries, ChartSeries)>( changes =>
                    Observable.Start( () =>
                    {
                        var incomes = changes.Where( x => x.Reason == ChangeReason.Add )
                            .Select( x => x.Current )
                            .ToDictionary( x => x.Period , x => x.Income );
                        var expenses = changes.Where( x => x.Reason == ChangeReason.Add )
                            .Select( x => x.Current )
                            .ToDictionary( x => x.Period , x => x.Expense );
                        return (new ChartSeries
                        {
                            Title = "Income" ,
                            DataSource = incomes
                        },
                        new ChartSeries
                        {
                            Title = "Expense" ,
                            DataSource = expenses
                        });
                    } ) );

            @this.BuildEvolutionsSeriesCommand = ReactiveCommand
                .CreateFromObservable<(double, TransactionViewModel[]) , ChartSeries>( t =>
                    Observable.Start( () =>
                    {
                        var (startAmount, transactions) = t;
                        var data = new LinkedList<(DateTime date, double amount)>();
                        var _ = transactions.Where( o => o.IsExecuted )
                            .OrderBy( o => o.Date )
                            .GroupBy( o => o.Date )
                            .Aggregate( data , ( list , current ) =>
                            {
                                var previous = list.Count == 0 ? startAmount : list.Last.Value.amount;
                                list.AddLast( (current.Key, previous + current.Sum( o => o.Value )) );
                                return list;
                            } );
                        if ( data.Count > 0 )
                        {
                            var (date, amount) = data.Last.Value;
                            if ( date < DateTime.Today )
                                data.AddLast( (DateTime.Today, amount) );
                        }
                        return new ChartSeries
                        {
                            Title = "Total" ,
                            DataSource = data.ToDictionary( o => o.date , o => o.amount )
                        };
                    } ) );
        }
    }
}