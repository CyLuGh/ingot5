﻿using Ingots.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Ingots.ViewModels.CsvManagement
{
    public class FuzzyOperationComparer : IEqualityComparer<Operation>
    {
        public bool Equals( Operation? x , Operation? y )
        {
            if ( x != null && y != null )
            {
                if ( x is Transfer t1 && y is Transfer t2 && t1.TargetAccount != t2.TargetAccount )
                    return false;

                return x.Value == y.Value
                    && Math.Abs( ( y.Date - x.Date ).TotalDays ) <= 3;
            }

            return false;
        }

        public int GetHashCode( [DisallowNull] Operation obj )
            => 0; // Disable check on hashcode
    }
}