﻿using Ingots.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;

namespace Ingots.Views
{
    public partial class TransactionDataGridIconView
    {
        public TransactionDataGridIconView()
        {
            InitializeComponent();
            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( x => PopulateFromViewModel( this , x , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( TransactionDataGridIconView view , TransactionViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.Value ,
                v => v.Icon.Foreground ,
                val => val < 0 ? Brushes.IndianRed : Brushes.SeaGreen )
            .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Value ,
                v => v.Icon.Kind ,
                val => val < 0 ? PackIconKind.ArrowDownBoldBoxOutline : PackIconKind.ArrowUpBoldBoxOutline  )
            .DisposeWith( disposables );
        }
    }
}